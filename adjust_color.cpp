#include <math.h>
#include "snoop.h"
#include "adjust_color.h"

static void fill_adjustmentLUT(unsigned char *lut, int x1, int y1, int x2, int y2);

void
set_brightnessLUT(int amount)
{     
	amount=(int)(amount*255.0/100.0+0.5);

	if (amount<0)
		fill_adjustmentLUT(global_snoop->brightnessLUT, 0, 0, 255, 255+amount);
	else {
		if (amount>255) amount=255;
		fill_adjustmentLUT(global_snoop->brightnessLUT, 0, amount, 255, 255);
	}
}

void
set_contrastLUT(int amount)
{     
	int x;

	// remapping from UI to real amount
	amount=(int)(amount*128.0/100.0+0.5);
	unsigned char *lut = global_snoop->contrastLUT;

	if (amount<0)
		fill_adjustmentLUT(lut, 0, -amount, 255, 255+amount);
	else {
		if (amount>255) amount=255;                             
		for (x=0; x<amount; x++) lut[x]=0;
		fill_adjustmentLUT(lut, amount, 0, 255-amount, 255);
		for (x=255; x>255-amount; x--) lut[x]=255;
	}
}

void
set_gammaLUT(double amount)
{     
	double y;
	int z, i;
	unsigned char *lut = global_snoop->gammaLUT;

	if (amount>=0) {
		lut[0]=0;
		y=1/amount;
		for (i=1; i<256; i++) {
			z=(int)(255*exp(y*log(i/255.0))+0.5);
			if (z>255) z=255;
			else if (z<0) z=0;
			lut[i]=z;
		}
	}
}

void
adjust_color(GdkPixbuf *pixbuf)
{     
	int width = gdk_pixbuf_get_width(pixbuf);
	int height = gdk_pixbuf_get_height(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf);
	unsigned char *c = global_snoop->contrastLUT;
	unsigned char *b = global_snoop->brightnessLUT;
	unsigned char *g = global_snoop->gammaLUT;
	guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);

	int x, y;
	for (y = 0; y < height; y++, pixels += rowstride) {
		guchar *p = pixels;
		for (x = 0; x < width; x++, p += n_channels) {
			p[0] = c[b[g[p[0]]]];
			p[1] = c[b[g[p[1]]]];
			p[2] = c[b[g[p[2]]]];
			// don't care alpha value
		}
	}
}

void
fill_adjustmentLUT(unsigned char *lut, int x1, int y1, int x2, int y2)
{         
	int n, i, ii, x;

	n=x2-x1;

	if (n==0) return;
	i =(y1 << 16);
	ii=( (y2-y1+1) << 16)/n;                                                          
	for (x=x1; x<=x2; x++) {
		lut[x]=i>>16;                                                                             i+=ii;
	}
}

