#include "draw.h"

static const int highlight_value = 40;

void
draw_center(GdkPixbuf *pixbuf, int center_x, int center_y) // {{{
{
	int factor = global_snoop->zoom_factor;
	if (factor == 1) return;
	int x = center_x, y = center_y;
	int left = x*factor, top = y*factor;
	int right = left + factor, bottom = top + factor;
	
	draw_line(pixbuf, left, top, right, top, 255, 0, 0, -1);
	draw_line(pixbuf, left, bottom, right, bottom, 255, 0, 0, -1);
	draw_line(pixbuf, left, top, left, bottom, 255, 0, 0, -1);
	draw_line(pixbuf, right, top, right, bottom, 255, 0, 0, -1);
} // }}}

void
draw_hhighlight(GdkPixbuf *pixbuf, Geometry *g) // {{{
{
	int y = g->center_y;
	int top = y*g->zoom_factor;
	int bottom = top + g->zoom_factor;
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	guchar *pixels = gdk_pixbuf_get_pixels(pixbuf) + top*rowstride;
	
	int i, j;
	for (j = top; j < bottom; j++, pixels += rowstride) {
		guchar *p = pixels;
		for (i = 0; i < g->image_width; i++, p+=n_channels) {
			p[0] = (p[0] + highlight_value) <= 255 ? p[0] + highlight_value : 255;
			p[1] = (p[1] + highlight_value) <= 255 ? p[1] + highlight_value : 255;
			p[2] = (p[2] + highlight_value) <= 255 ? p[2] + highlight_value : 255;
		}
	}
} // }}}

void
draw_vhighlight(GdkPixbuf *pixbuf, Geometry *g) // {{{
{
	int x = g->center_x; 
	int left = x*g->zoom_factor; 
	int right = left + g->zoom_factor;
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	guchar *pixels = gdk_pixbuf_get_pixels(pixbuf) + left*n_channels;
	
	int i, j;
	for (j = 0; j < g->image_height; j++, pixels += rowstride) {
		guchar *p = pixels;
		for (i = left; i < right; i++, p+=n_channels) {
			p[0] = (p[0] + highlight_value) <= 255 ? p[0] + highlight_value : 255;
			p[1] = (p[1] + highlight_value) <= 255 ? p[1] + highlight_value : 255;
			p[2] = (p[2] + highlight_value) <= 255 ? p[2] + highlight_value : 255;
		}
	}
} // }}}

void
draw_grid(GdkPixbuf *pixbuf, int zoom_factor) // {{{
{
	if (zoom_factor == 1) return;

	int width = gdk_pixbuf_get_width (pixbuf);
	int height = gdk_pixbuf_get_height (pixbuf);
	int x, y;

	for (y = 0; y < height; y += zoom_factor)
		draw_line(pixbuf, 0, y, width - 1, y, 0, 0, 0, -1);
	for (x = 0; x < width; x += zoom_factor)
		draw_line(pixbuf, x, 0, x, height - 1, 0, 0, 0, -1);
} // }}}
