#ifndef __IMAGE_H__
#define __IMAGE_H__

#include "snoop.h"

typedef struct { // {{{
	int screen_width, screen_height;
	int image_width, image_height;
	int desire_capture_width, desire_capture_height;
	int capture_left, capture_top; 
	int capture_width, capture_height;
	int pointer_x, pointer_y;
	int center_x, center_y;
	int zoom_factor;

	int need_fill;
	int src_x, src_y;

	GdkDisplay *display;
	GdkScreen *screen;
	GdkWindow *root_window;
} Geometry; // }}}

// image processing functions
void fill_color(GdkPixbuf *pixbuf, int r, int g, int b, int alpha);
void draw_line(GdkPixbuf *pixbuf, int x1, int y1, int x2, int y2, int r, int g, int b, int alpha);
void draw_soft_line(GdkPixbuf *pixbuf, int x1, int y1, int x2, int y2, int r, int g, int b, int alpha);
void draw_hline(GdkPixbuf *pixbuf, int y, int x1, int x2, int r, int g, int b, int alpha);
void draw_vline(GdkPixbuf *pixbuf, int x, int y1, int y2, int r, int g, int b, int alpha);
void prepare_coordinate(Geometry*);
void update_status(GdkPixbuf *, Geometry *);
void replace_image(GdkPixbuf *base, GdkPixbuf *img, int x0, int y0);
void scale_image(GdkPixbuf *src, GdkPixbuf *dst, int scale);

#endif
