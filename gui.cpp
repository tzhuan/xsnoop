#include <gdk/gdkkeysyms.h>
#include "gui.h"

#define EVENT_METHOD(i, x) GTK_WIDGET_GET_CLASS(i)->x

void main_window_init();
void time_window_init();
void display_window_init();
void main_window_accel_init();

gboolean
timer_event(gpointer data) // {{{
{
	Snoop *s = global_snoop;
	switch (s->timer_state) {
		case TIMER_RUNNING:
			break;
		case TIMER_STOP:
			return FALSE;
		case TIMER_RESTART:
			s->timer_state = TIMER_RUNNING;
			g_timeout_add(s->refresh_interval, timer_event, NULL);
			return FALSE;
		default:
			return FALSE;
	}
	update();
	return TRUE;
} // }}}

void
gui_init() // {{{
{
	Snoop *s = global_snoop;

	main_window_init();

	// set the icon
	GdkPixbuf *icon = gdk_pixbuf_new_from_inline(-1, snoop_icon_pixbuf, FALSE, 0);
	gtk_window_set_icon(GTK_WINDOW(s->main_window), icon);
	g_object_unref(icon);
	gtk_widget_show_all(s->main_window);
	gtk_window_set_keep_above((GtkWindow*)s->main_window, s->on_top);

	update_title();

	if (s->ruler) {
		gtk_check_menu_item_set_active(
			(GtkCheckMenuItem*)(s->item_ruler), TRUE);
		put_ruler();
	} else {
		gtk_check_menu_item_set_active(
			(GtkCheckMenuItem*)(s->item_ruler), FALSE);
		unput_ruler();
	}

	if (s->grab_mode == MOUSE_ARROW_MODE) {
		s->timer_state = TIMER_RUNNING;
		g_timeout_add(s->refresh_interval, timer_event, NULL);
	}

	time_window_init();
	display_window_init();
	gtk_toggle_button_set_active((GtkToggleButton*)s->item_red, s->red);
	gtk_toggle_button_set_active((GtkToggleButton*)s->item_green, s->green);
	gtk_toggle_button_set_active((GtkToggleButton*)s->item_blue, s->blue);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(s->item_histogram_normal), 
			s->histogram_normal);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(s->item_gradient_normal), 
			s->gradient_normal);
	gtk_range_set_value(GTK_RANGE(s->item_gamma), s->gamma);
	gtk_range_set_value(GTK_RANGE(s->item_brightness), s->brightness);
	gtk_range_set_value(GTK_RANGE(s->item_contrast), s->contrast);
} // }}}

void
main_window_init() // {{{
{
	// create a new window
	Snoop *snoop = global_snoop;
	GtkWidget *window = snoop->main_window = 
		gtk_window_new (GTK_WINDOW_TOPLEVEL);
	//gtk_widget_set_size_request (GTK_WIDGET (window), 200, 200);
	gtk_window_set_default_size(GTK_WINDOW(window), snoop->width, snoop->height);

	g_signal_connect(G_OBJECT(window), "delete_event",
			G_CALLBACK(handle_delete), NULL);
	g_signal_connect(G_OBJECT(window), "destroy",
			G_CALLBACK(gtk_main_quit), NULL);
	g_signal_connect(G_OBJECT(window), "configure_event", 
			G_CALLBACK(handle_configure), NULL);
	g_signal_connect(G_OBJECT(window), "key_press_event", 
			G_CALLBACK(handle_key), NULL);
	g_signal_connect(G_OBJECT(window), "button_press_event", 
			G_CALLBACK(handle_button_press), NULL);
	g_signal_connect(G_OBJECT(window), "button_release_event", 
			G_CALLBACK(handle_button_release), NULL);
	/*
	g_signal_connect(G_OBJECT(window), "motion_notify_event", 
			G_CALLBACK(handle_motion_notify), NULL);
			*/

	GtkWidget *vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);

	GtkWidget *menu_bar = menu_init();
	gtk_box_pack_start(GTK_BOX(vbox), menu_bar, FALSE, FALSE, 0);

	// We have to pack GtkImage inside a EventBox to recieve events
	// and clip image.
	GtkWidget *event_box = gtk_event_box_new();
	GtkWidget *image = snoop->image = gtk_image_new();
	/*
	gtk_box_pack_start(GTK_BOX(vbox), event_box, TRUE, TRUE, 0);
	gtk_container_add(GTK_CONTAINER(event_box), image);
	*/

	GtkWidget *table = gtk_table_new(3, 3, FALSE);
	gtk_box_pack_start(GTK_BOX(vbox), table, TRUE, TRUE, 0);

	GtkWidget *hruler = snoop->hruler = gtk_hruler_new();
	GtkWidget *vruler = snoop->vruler = gtk_vruler_new();
	gtk_table_attach (GTK_TABLE (table), hruler, 1, 2, 0, 1,
		static_cast<GtkAttachOptions>(GTK_FILL|GTK_EXPAND), GTK_SHRINK, 0, 0);
        gtk_table_attach (GTK_TABLE (table), vruler, 0, 1, 1, 2,
		static_cast<GtkAttachOptions>(GTK_SHRINK), static_cast<GtkAttachOptions>(GTK_FILL|GTK_EXPAND), 0, 0);

        gtk_table_attach(GTK_TABLE(table), event_box, 1, 2, 1, 2,
		static_cast<GtkAttachOptions>(GTK_FILL|GTK_EXPAND), static_cast<GtkAttachOptions>(GTK_FILL|GTK_EXPAND), 0, 0);

	gtk_container_add(GTK_CONTAINER(event_box), image);
	gtk_widget_set_size_request (GTK_WIDGET(image), 300, 240);
	
	gtk_widget_add_events(event_box, 
		GDK_POINTER_MOTION_MASK|GDK_POINTER_MOTION_HINT_MASK);
	g_signal_connect(G_OBJECT(event_box), 
		"motion_notify_event", G_CALLBACK(handle_motion_notify), NULL);
	/*
	g_signal_connect_swapped(G_OBJECT(image), "motion_notify_event", 
		G_CALLBACK(EVENT_METHOD(hruler, motion_notify_event)),
		G_OBJECT(hruler));
	g_signal_connect_swapped(G_OBJECT(image), "motion_notify_event", 
		G_CALLBACK(EVENT_METHOD(vruler, motion_notify_event)), 
		G_OBJECT(vruler));
	*/

	global_snoop->statusbar = gtk_statusbar_new();

	PangoFontDescription* font = pango_font_description_new();
	pango_font_description_set_family(font, "monospace");
	pango_font_description_set_size(font, 10*PANGO_SCALE);
	gtk_widget_modify_font(GTK_STATUSBAR(global_snoop->statusbar)->label, font);
	pango_font_description_free(font);

	global_snoop->status_cid = gtk_statusbar_get_context_id(
			(GtkStatusbar*)global_snoop->statusbar, "");
	gtk_statusbar_push((GtkStatusbar*)global_snoop->statusbar, 
			global_snoop->status_cid, "");

	gtk_box_pack_end(GTK_BOX(vbox), global_snoop->statusbar, FALSE, FALSE, 0);

	main_window_accel_init();
} // }}}

void
time_window_init() // {{{
{
	GtkWidget *dialog = global_snoop->time_window = 
		gtk_dialog_new_with_buttons(
			"Setting Refresh Rate",
			(GtkWindow*)global_snoop->main_window, 
			static_cast<GtkDialogFlags>(GTK_DIALOG_MODAL|GTK_DIALOG_DESTROY_WITH_PARENT), 
			GTK_STOCK_OK, GTK_RESPONSE_OK,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			NULL);
	GtkWidget *label = gtk_label_new("interval (1/1000 second): ");
	GtkWidget *spin = global_snoop->item_refresh_interval = 
		gtk_spin_button_new_with_range(20, INT_MAX, 10);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin), global_snoop->refresh_interval);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), label);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), spin);
} // }}}

void
display_window_init() // {{{
{
	Snoop *s = global_snoop;
	GtkWidget *dialog = 
		s->display_window = 
		gtk_dialog_new_with_buttons(
			"Display Options",
			(GtkWindow*)global_snoop->main_window, 
			GTK_DIALOG_MODAL, 
			GTK_STOCK_OK,
			GTK_RESPONSE_OK,
			GTK_STOCK_CANCEL,
			GTK_RESPONSE_CANCEL,
			NULL);

	GtkWidget *box, *box2, *table, *frame;

	// first row: check-box (red, green, blue)
	box = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), box, TRUE, FALSE, 0);

	GtkWidget *red = s->item_red = gtk_check_button_new_with_mnemonic("_Red");
	g_signal_connect(G_OBJECT(red), "toggled",
			G_CALLBACK(handle_check_button), &(s->red));

	GtkWidget *green = s->item_green = gtk_check_button_new_with_mnemonic("_Green");
	g_signal_connect(G_OBJECT(green), "toggled",
			G_CALLBACK(handle_check_button), &(s->green));

	GtkWidget *blue = s->item_blue = gtk_check_button_new_with_mnemonic("_Blue");
	g_signal_connect(G_OBJECT(blue), "toggled",
			G_CALLBACK(handle_check_button), &(s->blue));

	gtk_box_pack_start(GTK_BOX(box), red, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(box), green, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(box), blue, FALSE, FALSE, 0);

	// second row: normal and reference
	// normal: check-box (histogram, gradient)
	box = gtk_hbox_new(TRUE, 0);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox),
			box, TRUE, FALSE, 0);
	frame = gtk_frame_new("Auto Normalize");
	gtk_box_pack_start(GTK_BOX(box), frame, TRUE, TRUE, 0);
	box2 = gtk_vbox_new(TRUE, 0);
	gtk_container_add(GTK_CONTAINER(frame), box2);

	GtkWidget *histogram_normal = s->item_histogram_normal = 
		gtk_check_button_new_with_mnemonic("_Histogram");
	g_signal_connect(G_OBJECT(histogram_normal), "toggled",
			G_CALLBACK(handle_check_button), 
			&(s->histogram_normal));

	GtkWidget *gradient_normal = s->item_gradient_normal = 
		gtk_check_button_new_with_mnemonic("_Gradient");
	g_signal_connect(G_OBJECT(gradient_normal), "toggled",
			G_CALLBACK(handle_check_button), 
			&(s->gradient_normal));

	gtk_box_pack_start(GTK_BOX(box2), histogram_normal, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(box2), gradient_normal, FALSE, FALSE, 0);

	// reference: spin-button (histogram, gradient)
	frame = gtk_frame_new("reference");
	gtk_box_pack_start(GTK_BOX(box), frame, TRUE, TRUE, 0);
	GtkWidget *l_histogram = gtk_label_new("Histogram");
	GtkWidget *l_gradient = gtk_label_new("Gradient");

	GtkWidget *histogram_reference = s->item_histogram_reference =
		gtk_spin_button_new_with_range(10, 10000, 10);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(histogram_reference), s->histogram_reference);
	g_signal_connect(G_OBJECT(histogram_reference), "value-changed",
			G_CALLBACK(handle_spin_button), 
			&(global_snoop->histogram_reference));

	GtkWidget *gradient_reference = s->item_gradient_reference = 
		gtk_spin_button_new_with_range(10, 255, 1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(gradient_reference), s->gradient_reference);
	g_signal_connect(G_OBJECT(gradient_reference), "value-changed",
			G_CALLBACK(handle_spin_button), 
			&(global_snoop->gradient_reference));

	table = gtk_table_new(2, 2, FALSE);
	gtk_table_attach((GtkTable*)table, l_histogram, 0, 1, 0, 1, 
			GTK_SHRINK, GTK_EXPAND, 0, 0);
	gtk_table_attach((GtkTable*)table, l_gradient, 0, 1, 1, 2, 
			GTK_SHRINK, GTK_EXPAND, 0, 0);
	gtk_table_attach((GtkTable*)table, histogram_reference, 1, 2, 0, 1, 
			static_cast<GtkAttachOptions>(GTK_EXPAND | GTK_FILL), GTK_EXPAND, 0, 0);
	gtk_table_attach((GtkTable*)table, gradient_reference, 1, 2, 1, 2, 
			static_cast<GtkAttachOptions>(GTK_EXPAND | GTK_FILL), GTK_EXPAND, 0, 0);
	gtk_container_add(GTK_CONTAINER(frame), table);
	
	// third row: brightness/contrast/gamma
	frame = gtk_frame_new(NULL);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), frame, FALSE, FALSE, 0);
	table = gtk_table_new(3, 2, FALSE);
	gtk_container_add(GTK_CONTAINER(frame), table);
	gtk_table_set_col_spacing((GtkTable*)table, 1, 20);

	// brightness
	GtkWidget *l_brightness = gtk_label_new("Brightness");
	GtkWidget *brightness = s->item_brightness = 
		gtk_hscale_new_with_range(-100, 100, 1);
	//gtk_range_set_value(GTK_RANGE(brightness), 0);
	g_signal_connect(G_OBJECT(brightness), "value-changed",
			G_CALLBACK(handle_brightness), 
			&(global_snoop->brightness));

	gtk_table_attach((GtkTable*)table, l_brightness, 0, 1, 0, 1, 
			GTK_SHRINK, GTK_EXPAND, 0, 0);
	gtk_table_attach((GtkTable*)table, brightness, 1, 2, 0, 1, 
			static_cast<GtkAttachOptions>(GTK_EXPAND | GTK_FILL), GTK_EXPAND, 0, 0);

	// contrast
	GtkWidget *l_contrast = gtk_label_new("Contrast");
	GtkWidget *contrast = s->item_contrast = 
		gtk_hscale_new_with_range(-100, 100, 1);
	//gtk_range_set_value(GTK_RANGE(contrast), 0);
	g_signal_connect(G_OBJECT(contrast), "value-changed",
			G_CALLBACK(handle_contrast), 
			&(global_snoop->contrast));

	gtk_table_attach((GtkTable*)table, l_contrast, 0, 1, 1, 2, 
			GTK_SHRINK, GTK_EXPAND, 0, 0);
	gtk_table_attach((GtkTable*)table, contrast, 1, 2, 1, 2, 
			static_cast<GtkAttachOptions>(GTK_EXPAND | GTK_FILL), GTK_EXPAND, 0, 0);

	// gamma
	GtkWidget *l_gamma = gtk_label_new("Gamma");
	GtkWidget *gamma = s->item_gamma = 
		gtk_hscale_new_with_range(0, 2, 0.01);
	//gtk_range_set_value(GTK_RANGE(gamma), 1);
	g_signal_connect(G_OBJECT(gamma), "value-changed",
			G_CALLBACK(handle_gamma), 
			&(s->gamma));

	gtk_table_attach((GtkTable*)table, l_gamma, 0, 1, 2, 3, 
			GTK_SHRINK, GTK_EXPAND, 0, 0);
	gtk_table_attach((GtkTable*)table, gamma, 1, 2, 2, 3, 
			static_cast<GtkAttachOptions>(GTK_EXPAND | GTK_FILL), GTK_EXPAND, 0, 0);
} // }}}

void
update_title() // {{{
{
	char msg[256] = "";
	Snoop *s = global_snoop;
	int depth = gdk_drawable_get_depth(
		(GdkDrawable*)gdk_get_default_root_window());
	int screen_width = gdk_screen_width();
	int screen_height = gdk_screen_height();

	sprintf(msg, "snoop x%d [%c] %d-bit (%dx%d)",
		s->zoom_factor, (s->grab_mode == MOUSE_ARROW_MODE ? 'M' : 'D'),
		depth, screen_width, screen_height);
	gtk_window_set_title(GTK_WINDOW(s->main_window), msg);
} // }}}

void
update() // {{{
{
	static GdkPixbuf *bg = NULL;
	Snoop *s = global_snoop;
	Geometry geometry;
	Geometry *g = &geometry;

	prepare_coordinate(&geometry);

	// capture the screenshot
	if (!bg || gdk_pixbuf_get_width(bg) != g->desire_capture_width || 
		gdk_pixbuf_get_height(bg) != g->desire_capture_height) {
		if (bg)
			g_object_unref(bg);
		bg = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, 
			g->desire_capture_width, g->desire_capture_height);
	}

	fill_color(bg, 0, 0, 0, 255);

	GdkPixbuf *src_buffer = gdk_pixbuf_get_from_drawable(bg, 
			(GdkDrawable*)g->root_window, gdk_colormap_get_system(),
			g->capture_left, g->capture_top, 
			g->src_x, g->src_y, 
			g->capture_width, g->capture_height); 
	if (!src_buffer)
		return;

	// update status before filtering
	update_status(src_buffer, g);
	
	// apply filter
	apply_filter(s->filter, src_buffer);

	adjust_color(src_buffer);
	/* calculate statistics
	if(s->statistics)
	    update_statistics(src_buffer);
	*/

	// scale the screenshot
	//if (scaled_buffer) g_object_unref(tmp);
	GdkPixbuf *scaled_buffer = 
		gdk_pixbuf_new(
			gdk_pixbuf_get_colorspace(src_buffer), 
			gdk_pixbuf_get_has_alpha(src_buffer),
			gdk_pixbuf_get_bits_per_sample(src_buffer),
			g->image_width, g->image_height
		);

		scale_image(src_buffer, scaled_buffer, g->zoom_factor);

	if (s->grid) draw_grid(scaled_buffer, g->zoom_factor);

	if (s->center_highlight) 
		draw_center(scaled_buffer, g->center_x, g->center_y);

	if (s->highlight){
		switch (s->graph){ 
		    case HORIZONTAL_STEP: 
		    case HORIZONTAL_LINEAR:
			draw_hhighlight(scaled_buffer, g);
			break;

		    case VERTICAL_STEP:
		    case VERTICAL_LINEAR:
			draw_vhighlight(scaled_buffer, g);
			break;
		    
		    default:
			break;
		}
	}

	if (s->graph != NO_GRAPH) {
		if (s->graph == STATISTICS)
			update_statistics(g, src_buffer, scaled_buffer);
		else
			draw_graph(src_buffer, scaled_buffer, g->center_x, g->center_y);
	}

	// update image 
	gtk_image_set_from_pixbuf(GTK_IMAGE(s->image), scaled_buffer);

	// unreference GdkPixbuf
	g_object_unref(scaled_buffer);
} // }}}

void
put_ruler()
{
	//int w, h;
	//gtk_widget_get_size_request(global_snoop->vruler, &w, NULL);
	//gtk_widget_get_size_request(global_snoop->hruler, NULL, &h);
	gtk_widget_show(global_snoop->hruler);
	gtk_widget_show(global_snoop->vruler);
	update_ruler();
}

void
unput_ruler()
{
	gtk_widget_hide(global_snoop->hruler);
	gtk_widget_hide(global_snoop->vruler);
}

void
update_ruler()
{
	Snoop *s = global_snoop;
	int width = s->image->allocation.width;
	int height = s->image->allocation.height;
	//gtk_widget_get_size_request(s->image, &width, &height);
	double hupper = (double)width/s->zoom_factor;
	double vupper = (double)height/s->zoom_factor;
	gtk_ruler_set_range((GtkRuler*)s->hruler, 0, hupper, 0, hupper);
	gtk_ruler_set_range((GtkRuler*)s->vruler, 0, vupper, 0, vupper);
}

void
main_window_accel_init()
{
	Snoop *s = global_snoop;
	GtkAccelGroup *a = s->accel_group = gtk_accel_group_new();
        gtk_window_add_accel_group(GTK_WINDOW(s->main_window), a);
	gtk_widget_add_accelerator(s->item_on_top, "activate", a, GDK_F2, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_grid, "activate", a, GDK_F3, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	//gtk_widget_add_accelerator(s->item_hex_mode, "activate", a, GDK_F3, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	//gtk_widget_add_accelerator(s->item_inverse_y, "activate", a, GDK_F3, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	//gtk_widget_add_accelerator(s->item_center_highlight, "activate", a, GDK_F3, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	//gtk_widget_add_accelerator(s->item_highlight, "activate", a, GDK_F3, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_ruler, "activate", a, GDK_r, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	//gtk_widget_add_accelerator(s->item_filter[], "activate", a, GDK_F3, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_grab_mode[MOUSE_ARROW_MODE], "activate", a, GDK_m, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_grab_mode[DRAG_MODE], "activate", a, GDK_a, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_graph[NO_GRAPH], "activate", a, GDK_o, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_graph[HORIZONTAL_STEP], "activate", a, GDK_H, GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_graph[HORIZONTAL_LINEAR], "activate", a, GDK_h, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_graph[VERTICAL_STEP], "activate", a, GDK_V, GDK_SHIFT_MASK, GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_graph[VERTICAL_LINEAR], "activate", a, GDK_v, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_graph[STATISTICS], "activate", a, GDK_F4, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_quit, "activate", a, GDK_Escape, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
	gtk_widget_add_accelerator(s->item_update, "activate", a, GDK_u, static_cast<GdkModifierType>(0), GTK_ACCEL_VISIBLE); 
}

void set_graph_sensitive()
{
	Snoop *s = global_snoop;
	if (s->graph == NO_GRAPH) {
		gtk_widget_set_sensitive(s->item_highlight, FALSE);
		gtk_widget_set_sensitive(s->item_statistics_info, FALSE);
	} else if (s->graph == STATISTICS) {
		gtk_widget_set_sensitive(s->item_highlight, FALSE);
		gtk_widget_set_sensitive(s->item_statistics_info, TRUE);
	} else {
		gtk_widget_set_sensitive(s->item_highlight, TRUE);
		gtk_widget_set_sensitive(s->item_statistics_info, FALSE);
	}
}
