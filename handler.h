#ifndef __HANDLER_H__
#define __HANDLER_H__

#include "snoop.h"
#include "gui.h"

// check-box/radio/dialog handler
void handle_refresh_rate_dialog(GtkMenuItem *item, gpointer);
void handle_display_option_dialog(GtkMenuItem *item, gpointer);
void handle_option(GtkCheckMenuItem *item, gpointer data);
void handle_ruler(GtkCheckMenuItem *item, gpointer data);
void handle_ontop(GtkCheckMenuItem *item, gpointer data);
void handle_colorspace(GtkRadioMenuItem *item, gpointer data);
void handle_filter(GtkRadioMenuItem *item, gpointer data);
void handle_graph(GtkRadioMenuItem *item, gpointer data);
void handle_grab_mode(GtkRadioMenuItem *item, gpointer data);
void handle_saveconfig(GtkMenuItem *item, gpointer data);
gint handle_configure(GtkWindow *window, gpointer data);
gint handle_delete(GtkWindow *window, gpointer data);
void handle_check_button(GtkToggleButton *button, gpointer data);
void handle_spin_button(GtkSpinButton *spin, gpointer data);
void handle_brightness(GtkRange *range, gpointer data);
void handle_contrast(GtkRange *range, gpointer data);
void handle_gamma(GtkRange *range, gpointer data);
void handle_statistics_info(GtkCheckMenuItem* item, gpointer data);
void handle_copy(GtkMenuItem* item, gpointer data);
void handle_quit(GtkCheckMenuItem* item, gpointer data);
void handle_update(GtkCheckMenuItem* item, gpointer data);
void handle_about(GtkCheckMenuItem* item, gpointer data);

#endif
