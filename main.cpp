#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <gtk/gtk.h>

#include "snoop.h"
#include "gui.h"

#include "snoop_icon.h"

Snoop *global_snoop;

int
main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
	global_snoop = snoop_new();
	if (!global_snoop)
		exit(1);
	load_config();
	gui_init();
	gtk_main();
	if(global_snoop->autosave)
	    save_config();
	snoop_destroy(global_snoop);
	return 0;
}

