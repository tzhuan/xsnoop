#include "handler.h"

void
handle_quit(GtkCheckMenuItem *item, gpointer data)
{
	global_snoop->timer_state = TIMER_STOP;	
	gtk_main_quit ();
}

void
handle_update(GtkCheckMenuItem *item, gpointer data)
{
	update();
}

void
handle_about(GtkCheckMenuItem *item, gpointer data)
{
	Snoop *s = global_snoop;
	const gchar *(authors[]) = {
		"Tz-Huan Huang <tzhuan@cmlab.csie.ntu.edu.tw>",
		"Shan-yung Yang <littleshan@cmlab.csie.ntu.edu.tw>",
		0
	};
	const gchar *message = 
		"Snoop for X, base on Yung-Yu Chuang's snoop\n\n"
		"[1-9] zoom factor = n\n"
		"[0] zoom factor = 10\n"
		"[=+] increase zoom factor\n"
		"[-] decrease zoom factor\n"
		"[Esc] quit\n"
		"(other hotkeys list in menu-item)";

	GdkPixbuf *icon = gdk_pixbuf_new_from_inline(-1, snoop_icon_pixbuf, FALSE, 0);
	GdkPixbuf *big_icon = gdk_pixbuf_scale_simple(icon, 128, 128, GDK_INTERP_NEAREST);
	// draw_grid(big_icon, gdk_pixbuf_get_width(big_icon)/gdk_pixbuf_get_width(icon));
	gtk_show_about_dialog(
		GTK_WINDOW(s->main_window), 
		"program-name", "Snoop",
		"logo", big_icon,
		"title", "About snoop",
		"authors", authors,
		"comments", message,
		0
	);
	g_object_unref(big_icon);
	g_object_unref(icon);
}

void
handle_option(GtkCheckMenuItem *item, gpointer data) // {{{
{
	*(bool*)data = gtk_check_menu_item_get_active(item);
} // }}}

void
handle_check_button(GtkToggleButton *button, gpointer data) // {{{
{
	*(bool*)data = gtk_toggle_button_get_active(button);
} // }}}

void 
handle_spin_button(GtkSpinButton *spin, gpointer data) // {{{
{
	*(int*)data = gtk_spin_button_get_value_as_int(spin);
} // }}}

void
handle_brightness(GtkRange *range, gpointer data) // {{{
{
	
	set_brightnessLUT(*(int*)data = gtk_range_get_value(range));
} // }}}

void
handle_contrast(GtkRange *range, gpointer data) // {{{
{
	
	set_contrastLUT(*(int*)data = gtk_range_get_value(range));
} // }}}

void
handle_gamma(GtkRange *range, gpointer data) // {{{
{
	set_gammaLUT(*(double*)data = gtk_range_get_value(range));
} // }}}

void
handle_ruler(GtkCheckMenuItem *item, gpointer data) // {{{
{
	*(bool*)data = gtk_check_menu_item_get_active(item);
	
	if (*(bool*)data)
		put_ruler();
	else
		unput_ruler();
} // }}}

int calc_index(GtkWidget **base, gpointer data) // {{{
{
	int index = (static_cast<GtkWidget**>(data)-base);
	return index;
} // }}}

void
handle_filter(GtkRadioMenuItem *item, gpointer data) // {{{
{
	int index = calc_index(global_snoop->item_filter, data);
	global_snoop->filter = static_cast<Filter>(index);
} // }}}

void
handle_colorspace(GtkRadioMenuItem *item, gpointer data) // {{{
{
	int index = calc_index(global_snoop->item_colorspace, data);
	global_snoop->colorspace = static_cast<Colorspace>(index);
} // }}}

void
handle_refresh_rate_dialog(GtkMenuItem *menuitem, gpointer spin) // {{{
{
	Snoop *s = global_snoop;
	gtk_dialog_set_default_response(GTK_DIALOG(s->time_window), GTK_RESPONSE_OK);
	gtk_widget_show_all(s->time_window);
	gint result = gtk_dialog_run(GTK_DIALOG(s->time_window));
	if (result == GTK_RESPONSE_OK) {
		gint interval = gtk_spin_button_get_value_as_int
			(GTK_SPIN_BUTTON(s->item_refresh_interval));
		if(interval >= 20 && interval < INT_MAX){
			s->refresh_interval = interval;
			s->timer_state = TIMER_RESTART;
		}
	} else {
		gtk_spin_button_set_value(
			GTK_SPIN_BUTTON(s->item_refresh_interval), s->refresh_interval);
	}
	gtk_widget_hide_all(global_snoop->time_window);
} // }}}

void
handle_display_option_dialog(GtkMenuItem *menuitem, gpointer data) // {{{
{
	Snoop *s = global_snoop;
	// save the current display options
	bool red = s->red;
	bool green = s->green;
	bool blue = s->blue;
	bool histogram_normal = s->histogram_normal;
	bool gradient_normal = s->gradient_normal;
	int histogram_reference = s->histogram_reference;
	int gradient_reference = s->gradient_reference;
	int brightness = s->brightness;
	int contrast = s->contrast;
	double gamma = s->gamma;

	gtk_widget_show_all(global_snoop->display_window);
	gint result = gtk_dialog_run((GtkDialog*)global_snoop->display_window);
	if (result == GTK_RESPONSE_CANCEL) {
		gtk_toggle_button_set_active((GtkToggleButton*)s->item_red, red);
		gtk_toggle_button_set_active((GtkToggleButton*)s->item_green, green);
		gtk_toggle_button_set_active((GtkToggleButton*)s->item_blue, blue);
		gtk_toggle_button_set_active((GtkToggleButton*)s->item_histogram_normal, histogram_normal);
		gtk_toggle_button_set_active((GtkToggleButton*)s->item_gradient_normal, gradient_normal);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(s->item_histogram_reference), histogram_reference);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(s->item_gradient_reference), gradient_reference);
		gtk_range_set_value(GTK_RANGE(s->item_brightness), brightness);
		gtk_range_set_value(GTK_RANGE(s->item_contrast), contrast);
		gtk_range_set_value(GTK_RANGE(s->item_gamma), gamma);
	}
	gtk_widget_hide_all(global_snoop->display_window);
} // }}}

void
handle_graph(GtkRadioMenuItem *item, gpointer data) // {{{
{
	int index = calc_index(global_snoop->item_graph, data);
	Snoop *s = global_snoop;
	s->graph = static_cast<Graph>(index);
	set_graph_sensitive();
} // }}}

void
handle_grab_mode(GtkRadioMenuItem *item, gpointer data) // {{{
{
	int index = calc_index(global_snoop->item_grab_mode, data);
	global_snoop->grab_mode = static_cast<GrabMode>(index);
	update_title();
	if (global_snoop->grab_mode == DRAG_MODE)
		global_snoop->timer_state = TIMER_STOP;	
	else if (global_snoop->grab_mode == MOUSE_ARROW_MODE) {
		global_snoop->timer_state = TIMER_RUNNING;
		g_timeout_add(global_snoop->refresh_interval, timer_event, NULL);
	}
} // }}}

void 
handle_saveconfig(GtkMenuItem *item, gpointer data)
{
	save_config();
}

void
handle_ontop(GtkCheckMenuItem* item, gpointer data)
{
	global_snoop->on_top = gtk_check_menu_item_get_active(item);
	gtk_window_set_keep_above((GtkWindow*)global_snoop->main_window, global_snoop->on_top);
}

gint 
handle_configure(GtkWindow* window, gpointer data)
{
	gtk_window_get_size(window, &(global_snoop->width), &(global_snoop->height));
	update_ruler();
	return FALSE;
}

gint 
handle_delete(GtkWindow* window, gpointer data)
{
	global_snoop->timer_state = TIMER_STOP;	
	gtk_main_quit ();
	return FALSE;
}

void handle_statistics_info(GtkCheckMenuItem* item, gpointer data){
    Snoop* s = global_snoop;
    // toggle statistics_info on/off
    s->statistics_info = gtk_check_menu_item_get_active(item);
}

void handle_copy(GtkMenuItem* item, gpointer data){
    // copy the image into the clipboard
    //GtkClipboard* clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
    //GdkPixbuf* pixbuf = gtk_image_get_pixbuf(GTK_IMAGE(global_snoop->image));
    // FIXME gtk_clipboard_set_image requires Gtk-2.6!
    //gtk_clipboard_set_image(clipboard, pixbuf);
}
