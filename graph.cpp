#include <pango/pangocairo.h>

#include "snoop.h"
#include "image.h"
#include "graph.h"

static const int space = 6;

void
draw_graph(GdkPixbuf *src, GdkPixbuf *pixbuf, int center_x, int center_y) // {{{
{
	int left = 0, right = 0, top = 0, bottom = 0; 
	int image_width = global_snoop->image->allocation.width;
	int image_height = global_snoop->image->allocation.height;
	Graph graph = global_snoop->graph;

	if (graph == HORIZONTAL_STEP || graph == HORIZONTAL_LINEAR) {
		left = space;
		top = space;
		right = image_width - space;
		bottom = image_height/3;
	} else if (graph == VERTICAL_STEP || graph == VERTICAL_LINEAR) {
		left = 2*image_width/3;
		top = space;
		right = image_width - space;
		bottom = image_height - space;
	}

	// draw black background
	int y;
	for (y = top; y <= bottom; y++)
		draw_line(pixbuf, left, y, right, y, 0, 0, 0, -1);

	// draw graph
	if (graph == HORIZONTAL_STEP)
		draw_hsgraph(src, pixbuf, center_y, top, left, right, bottom);
	else if (graph == HORIZONTAL_LINEAR)
		draw_hlgraph(src, pixbuf, center_y, top, left, right, bottom);
	else if (graph == VERTICAL_STEP)
		draw_vsgraph(src, pixbuf, center_x, top, left, right, bottom);
	else if (graph == VERTICAL_LINEAR)
		draw_vlgraph(src, pixbuf, center_x, top, left, right, bottom);

	// draw border
	draw_line(pixbuf, left, top, right, top, 255, 255, 255, -1);
	draw_line(pixbuf, left, bottom, right, bottom, 255, 255, 255, -1);
	draw_line(pixbuf, left, top, left, bottom, 255, 255, 255, -1);
	draw_line(pixbuf, right, top, right, bottom, 255, 255, 255, -1);
} // }}}

void
draw_hsgraph(GdkPixbuf *src, GdkPixbuf *pixbuf, int center_y, int top, int left, int right, int bottom) // {{{
{
	int y = center_y;
	int n_channels = gdk_pixbuf_get_n_channels(src);
	int rowstride = gdk_pixbuf_get_rowstride(src);
	int height = bottom - top + 1, x;
	int desire_capture_width = gdk_pixbuf_get_width(src);
	int factor = global_snoop->zoom_factor;
	guchar *pixels = gdk_pixbuf_get_pixels(src);

	guchar *p = pixels + y*rowstride;
	int line_left = 0, line_right = factor;
	for (x = 0; x < desire_capture_width; x++, p += n_channels, 
		line_left += factor, line_right += factor) {
		int _left = line_left, _right = line_right;
		if (_right < left) continue;
		if (_left < left) _left = left;
		if (_left > right) break;
		if (_right > right) _right = right;
		int _y = bottom - (height-2)*p[0]/256 - 1;
		if (global_snoop->red)
			draw_line(pixbuf, _left, _y, _right, _y, 255, -1, -1, -1);
		_y = bottom - (height-2)*p[1]/256 - 1;
		if (global_snoop->green)
			draw_line(pixbuf, _left, _y, _right, _y, -1, 255, -1, -1);
		_y = bottom - (height-2)*p[2]/256 - 1;
		if (global_snoop->blue)
			draw_line(pixbuf, _left, _y, _right, _y, -1, -1, 255, -1);
	}
} // }}}

void
draw_hlgraph(GdkPixbuf *src, GdkPixbuf *pixbuf, int center_y, int top, int left, int right, int bottom) // {{{
{
	int y = center_y;
	int n_channels = gdk_pixbuf_get_n_channels(src);
	int rowstride = gdk_pixbuf_get_rowstride(src);
	int height = bottom - top + 1, x;
	int factor = global_snoop->zoom_factor;
	int desire_capture_width = gdk_pixbuf_get_width(src);
	guchar *pixels = gdk_pixbuf_get_pixels(src);

	guchar *p = pixels + y*rowstride;
	int line_left = -factor/2, line_right = -line_left;
	int _pre_r = bottom, _pre_g = bottom, _pre_b = bottom;
	int first = 1;
	for (x = 0; x < desire_capture_width; x++, p += n_channels, 
		line_left += factor, line_right += factor) {
		int _left = line_left, _right = line_right;
		if (_right < left) continue;
		if (_left < left) _left = left;
		if (_left > right) break;
		if (_right > right) _right = right;
		int _y = bottom - (height-2)*p[0]/256 - 1;
		if (!first && global_snoop->red)
			draw_line(pixbuf, _left, _pre_r, _right, _y, 255, -1, -1, -1);
		_pre_r = _y;
		_y = bottom - (height-2)*p[1]/256 - 1;
		if (!first && global_snoop->green)
			draw_line(pixbuf, _left, _pre_g, _right, _y, -1, 255, -1, -1);
		_pre_g = _y;
		_y = bottom - (height-2)*p[2]/256 - 1;
		if (!first && global_snoop->blue)
			draw_line(pixbuf, _left, _pre_b, _right, _y, -1, -1, 255, -1);
		_pre_b = _y;
		if (first)
			first = 0;
	}
} // }}}

void
draw_vsgraph(GdkPixbuf *src, GdkPixbuf *pixbuf, int center_x, int top, int left, int right, int bottom) // {{{
{
	int x = center_x;
	int n_channels = gdk_pixbuf_get_n_channels(src);
	int rowstride = gdk_pixbuf_get_rowstride(src);
	int width = right - left + 1, y;
	int factor = global_snoop->zoom_factor;
	int desire_capture_height = gdk_pixbuf_get_height(src);
	guchar *pixels = gdk_pixbuf_get_pixels(src);

	guchar *p = pixels + x*n_channels;
	int line_top = 0, line_bottom = factor;
	for (y = 0; y < desire_capture_height; y++, p += rowstride, 
		line_top += factor, line_bottom += factor) {
		int _top = line_top, _bottom = line_bottom;
		if (_bottom < top) continue;
		if (_top < top) _top = top;
		if (_top > bottom) break;
		if (_bottom > bottom) _bottom = bottom;
		int _x = left + (width-2)*p[0]/256 + 1;
		if (global_snoop->red)
			draw_line(pixbuf, _x, _top, _x, _bottom, 255, -1, -1, -1);
		_x = left + (width-2)*p[1]/256 + 1;
		if (global_snoop->green)
			draw_line(pixbuf, _x, _top, _x, _bottom, -1, 255, -1, -1);
		_x = left + (width-2)*p[2]/256 + 1;
		if (global_snoop->blue)
			draw_line(pixbuf, _x, _top, _x, _bottom, -1, -1, 255, -1);
	}
} // }}}

void
draw_vlgraph(GdkPixbuf *src, GdkPixbuf *pixbuf, int center_x, int top, int left, int right, int bottom) // {{{
{
	int x = center_x;
	int n_channels = gdk_pixbuf_get_n_channels(src);
	int rowstride = gdk_pixbuf_get_rowstride(src);
	int width = right - left + 1, y;
	int factor = global_snoop->zoom_factor;
	int desire_capture_height = gdk_pixbuf_get_height(src);
	guchar *pixels = gdk_pixbuf_get_pixels(src);

	guchar *p = pixels + x*n_channels;
	int line_top = -factor/2, line_bottom = -line_top;
	int _pre_r = left, _pre_g = left, _pre_b = left;
	int first = 1;
	for (y = 0; y < desire_capture_height; y++, p += rowstride, 
		line_top += factor, line_bottom += factor) {
		int _top = line_top, _bottom = line_bottom;
		if (_bottom < top) continue;
		if (_top < top) _top = top;
		if (_top > bottom) break;
		if (_bottom > bottom) _bottom = bottom;
		int _x = left + (width-2)*p[0]/256 + 1;
		if (!first && global_snoop->red)
			draw_line(pixbuf, _pre_r, _top, _x, _bottom, 255, -1, -1, -1);
		_pre_r = _x;
		_x = left + (width-2)*p[1]/256 + 1;
		if (!first && global_snoop->green)
			draw_line(pixbuf, _pre_g, _top, _x, _bottom, -1, 255, -1, -1);
		_pre_g = _x;
		_x = left + (width-2)*p[2]/256 + 1;
		if (!first && global_snoop->blue)
			draw_line(pixbuf, _pre_b, _top, _x, _bottom, -1, -1, 255, -1);
		_pre_b = _x;
		if (first)
			first = 0;
	}
} // }}}


// only used in draw_statgraph
static int max(int a, int b) // {{{
{
    return a > b ? a : b;
} // }}}

static void draw_channel_statgraph(GdkPixbuf* stat, int ch[256], int height, int peak, int color) // {{{
{
	int r=-1, g=-1, b=-1;
	switch(color){
		case 0:
			r = 255;
			break;
		case 1:
			g = 255;
			break;
		case 2:
			b = 255;
			break;
		default:
			break;
	}
	int i;
	int mean = (ch[0] + ch[1]) / 2;
	int y1 = max( height - ch[0]*height/peak, 0);
	int y2 = max( height - mean*height/peak, 0);
	draw_vline(stat, 0, y1, y2, r, g, b, -1);
	for(i = 1; i < 255; i++){
		y1 = max( height - ch[i]*height/peak, 0);
		draw_vline(stat, i, y1, y2, r, g, b, -1);

		mean = (ch[i+1] + ch[i]) / 2;
		y2 = max( height - mean*height/peak, 0);
		draw_vline(stat, i, y1, y2, r, g, b, -1);
	}
	y1 = max( height - ch[255]*height/peak, 0);
	draw_vline(stat, 255, y1, y2, r, g, b, -1);
} // }}}

static void draw_statgraph(Geometry *geometry, GdkPixbuf* dst, int red[256], int green[256], int blue[256], int peak) // {{{
{
	int width = 256;
	int height = 88;
	GdkPixbuf* stat = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, width, height);

	// fill background color
	fill_color(stat, 0, 0, 0, 255);
	draw_channel_statgraph(stat, red, height, peak, 0);
	draw_channel_statgraph(stat, green, height, peak, 1);
	draw_channel_statgraph(stat, blue, height, peak, 2);

	int dst_width = geometry->image_width;
	int x_shift = (dst_width-width)/2;
	int y_shift = 5;

	replace_image(dst, stat, x_shift, y_shift);
	draw_hline(dst, y_shift-1, x_shift-1, x_shift+width, 255, 255, 255, 255);
	draw_hline(dst, y_shift+height, x_shift-1, x_shift+width, 255, 255, 255, 255);
	draw_vline(dst, x_shift-1, y_shift-1, y_shift+height, 255, 255, 255, 255);
	draw_vline(dst, x_shift+width, y_shift-1, y_shift+height, 255, 255, 255, 255);
	g_object_unref(stat);
} // }}}

void draw_statistics_info(Geometry *geometry, GdkPixbuf* dst, const char *message, int line) // {{{
{
	int font_size = 9;
	GdkScreen *screen = gdk_screen_get_default();
	double resolution = gdk_screen_get_resolution(screen);

	int width = 290;
	int height = static_cast<int>(line*font_size*resolution/72) + 13;

	GdkPixbuf* text = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, width, height);

	cairo_surface_t *surface = cairo_image_surface_create_for_data(
		gdk_pixbuf_get_pixels(text),
		CAIRO_FORMAT_ARGB32,
		gdk_pixbuf_get_width(text),
		gdk_pixbuf_get_height(text),
		gdk_pixbuf_get_rowstride(text)
	);
	cairo_t *cr = cairo_create(surface);

	cairo_set_source_rgb(cr, .0, .0, .0);
	cairo_paint(cr);

	cairo_translate(cr, 2, 2);
	PangoLayout *layout = pango_cairo_create_layout(cr);

	pango_layout_set_text(layout, message, -1);
	PangoFontDescription *desc = pango_font_description_from_string("Monospace 9");
	pango_layout_set_font_description(layout, desc);
	pango_font_description_free(desc);

	cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
	pango_cairo_update_layout(cr, layout);
	pango_cairo_show_layout(cr, layout);

	g_object_unref(layout);

	cairo_destroy(cr);
	cairo_surface_destroy(surface);

	int dst_width = geometry->image_width;
	int dst_height = geometry->image_height;
	int x_shift = (dst_width-width)/2;
	int y_shift = dst_height - height - 5;

	replace_image(dst, text, x_shift, y_shift);
	draw_hline(dst, y_shift-1, x_shift-1, x_shift+width, 255, 255, 255, 255);
	draw_hline(dst, y_shift+height, x_shift-1, x_shift+width, 255, 255, 255, 255);
	draw_vline(dst, x_shift-1, y_shift-1, y_shift+height, 255, 255, 255, 255);
	draw_vline(dst, x_shift+width, y_shift-1, y_shift+height, 255, 255, 255, 255);
	g_object_unref(text);
} // }}}

void update_statistics(Geometry *geometry, GdkPixbuf* src, GdkPixbuf* dst) // {{{
{
	int red[256], green[256], blue[256];
	int i, j;

	for(i = 0; i < 256; i++){
		red[i] = 0;
		green[i] = 0;
		blue[i] = 0;
	}

	// save max, min, avg, med in array
	const int MAX=0, MIN=1, AVG=2, MED=3;
	int rinfo[4] = {0}, ginfo[4] = {0}, binfo[4] = {0};

	rinfo[MAX] = 0; ginfo[MAX] = 0; binfo[MAX] = 0;
	rinfo[MIN] = 255; ginfo[MIN] = 255; binfo[MIN] = 255;

	int rpeak=0, rpeak_value=0, rsum=0;
	int gpeak=0, gpeak_value=0, gsum=0;
	int bpeak=0, bpeak_value=0, bsum=0;

	const int width = gdk_pixbuf_get_width(src);
	const int height = gdk_pixbuf_get_height(src);
	const int rowstride = gdk_pixbuf_get_rowstride(src);
	const int channels = gdk_pixbuf_get_n_channels(src);
	const int pixel_number = width * height;
	guchar* pixels = gdk_pixbuf_get_pixels(src);
	guchar* p;

	for(i = 0; i < height; i++){
		for(j = 0; j < width; j++){
			p = pixels + i*rowstride + j*channels;
			red[ p[0] ]++;
			green[ p[1] ]++;
			blue[ p[2] ]++;
			rsum += p[0];
			gsum += p[1];
			bsum += p[2];

			if(rinfo[MAX] < p[0])
				rinfo[MAX] = p[0];
			if(rinfo[MIN] > p[0])
				rinfo[MIN] = p[0];

			if(ginfo[MAX] < p[1])
				ginfo[MAX] = p[1];
			if(ginfo[MIN] > p[1])
				ginfo[MIN] = p[1];

			if(binfo[MAX] < p[2])
				binfo[MAX] = p[2];
			if(binfo[MIN] > p[2])
				binfo[MIN] = p[2];
		}
	}

	rinfo[AVG] = rsum / pixel_number;
	ginfo[AVG] = gsum / pixel_number;
	binfo[AVG] = bsum / pixel_number;

	// calculate peaks and medians
	rsum = 0;
	gsum = 0;
	bsum = 0;
	for(i = 0; i < 256; i++){
		rsum += red[i];
		gsum += green[i];
		bsum += blue[i];
		if(rsum >= pixel_number/2){
			rinfo[MED] = i;
			rsum = -pixel_number;
		}
		if(gsum >= pixel_number/2){
			ginfo[MED] = i;
			gsum = -pixel_number;
		}
		if(bsum >= pixel_number/2){
			binfo[MED] = i;
			bsum = -pixel_number;
		}

		if(red[i] > rpeak_value){
			rpeak_value = red[i];
			rpeak = i;
		}
		if(green[i] > gpeak_value){
			gpeak_value = green[i];
			gpeak = i;
		}
		if(blue[i] > bpeak_value){
			bpeak_value = blue[i];
			bpeak = i;
		}
	}
	char message[256];
	Snoop* s = global_snoop;

	int line;
	if(s->filter == GRADIENT){
		snprintf(message, 256, "Min:(%03d)  Max:(%03d)  AVG:(%03d)",
				rinfo[MIN], rinfo[MAX], rinfo[AVG]);
		line = 1;
	}else{
		snprintf(message, 256, 
			"Peak:%03d(%d), %03d(%d), %03d(%d)\n"
			"Min:(%03d, %03d, %03d) Max:(%03d, %03d, %03d)\n"
			"Avg:(%03d, %03d, %03d) Med:(%03d, %03d, %03d)\n",
			rpeak, rpeak_value, gpeak, gpeak_value, bpeak, bpeak_value,
			rinfo[MIN], ginfo[MIN], binfo[MIN], rinfo[MAX], ginfo[MAX], binfo[MAX],
			rinfo[AVG], ginfo[AVG], binfo[AVG], rinfo[MED], ginfo[MED], binfo[MED]
		);
		line = 3;
	}


	int peak;
	if(s->filter == GRADIENT){
		peak = rpeak_value;
	}else{
		if(s->histogram_normal)
			peak = max(max(rpeak_value, gpeak_value), bpeak_value);
		else
			peak = s->histogram_reference;
	}

	if (s->statistics_info)
		draw_statistics_info(geometry, dst, message, line);
	draw_statgraph(geometry, dst, red, green, blue, peak);
} // }}}
