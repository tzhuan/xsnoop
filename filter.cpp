#include <stdlib.h>
#include <math.h>
#include "gui.h"

static void apply_gradient(GdkPixbuf*);

void apply_filter(Filter filter, GdkPixbuf* src){
    switch(filter){
	case NO_FILTER:
	    return;
	case GRADIENT:
	    apply_gradient(src);
	    break;
	default:
	    break;
    }
}

// returns sqrt( ( (a1-a2)^2 + (b1-b2)^2 ) / 2 )
static int rasq(int a1, int a2, int b1, int b2){
    double s = (a1-a2)*(a1-a2) + (b1-b2)*(b1-b2);
    return (int)sqrt(s/2);
}

void apply_gradient(GdkPixbuf* src){
    guchar* pixels = gdk_pixbuf_get_pixels(src);
    const int width = gdk_pixbuf_get_width(src);
    const int height = gdk_pixbuf_get_height(src);
    const int rowstride = gdk_pixbuf_get_rowstride(src);
    const int channels = gdk_pixbuf_get_n_channels(src);
    guchar *p, *g;
    guchar* grayscale;
    int intensity;

    grayscale = (guchar*)malloc(sizeof(guchar)*width*height);
    g = grayscale;
    
    // first pass, convert to greyscale
    int i, j;
    for(i = 0; i < height; i++){
	for(j = 0; j < width; j++){
	    p = pixels + i*rowstride + j*channels;
	    // FIXME: I = (2R + 5G + B) / 8
	    intensity = p[0]*2 + p[1]*5 + p[2];
	    *g = intensity / 8;
	    g++;
	}
    }
    
    // four corner pixels
    p = pixels;
    g = grayscale;
    p[0] = p[1] = p[2] = rasq(g[1]*3+g[1+width], g[0]*3+g[width], 
	    g[width]*3+g[1+width], g[0]*3+g[1]) / 4;
    
    p += (width-1) * channels;
    g += width - 1;
    p[0] = p[1] = p[2] = rasq(g[-1]*3+g[-1+width], g[0]*3+g[width], 
	    g[width]*3+g[-1+width], g[0]*2+g[-1]) / 4;
    
    p = pixels + (height-1) * rowstride;
    g = grayscale + (height-1) * width;
    p[0] = p[1] = p[2] = rasq(g[1]*3+g[1-width], g[0]*3+g[-width], 
	    g[-width]*3+g[1-width], g[0]*2+g[1]) / 4;
    
    p += (width-1) * channels;
    g += width - 1;
    p[0] = p[1] = p[2] = rasq(g[-1]*3+g[-1-width], g[0]*3+g[-width], 
	    g[-width]*3+g[-1-width], g[0]*3+g[-1]) / 4;
    
    for(j = 1; j < width-1; j++){
	p = pixels + j*channels;
	g = grayscale + j;
	p[0] = p[1] = p[2] = rasq(g[1]*3+g[1+width], g[-1]*3+g[-1+width], 
		g[1+width]+g[width]*2+g[-1+width], g[1]+g[0]*2+g[-1]) / 4;
	p += (height-1) * rowstride;
	g += (height-1) * width;
	p[0] = p[1] = p[2] = rasq(g[1]*3+g[1-width], g[-1]*3+g[-1-width], 
		g[1-width]+g[-width]*2+g[-1-width], g[1]+g[0]*2+g[-1]) / 4;
    }
    
    for(i = 1; i < height-1; i++){
	g = grayscale + i*width;
	p = pixels + i*rowstride;
	p[0] = p[1] = p[2] = rasq(g[1-width]+g[1]*2+g[1+width], g[-width]+g[0]*2+g[+width], 
		g[width]*3+g[1+width], g[-width]*3+g[1-width]) / 4;
	g++;
	p += channels;
	for(j = 1; j < width-1; j++){
	    p[0] = p[1] = p[2] = rasq(g[1-width]+g[1]*2+g[1+width], g[-1-width]+g[-1]*2+g[-1+width], 
		g[-1+width]+g[width]*2+g[1+width], g[-1-width]+g[-width]*2+g[1-width]) / 4;
	    g++;
	    p += channels;
	}
	p[0] = p[1] = p[2] = rasq(g[-1-width]+g[-1]*2+g[-1+width], g[-width]+g[0]*2+g[+width], 
		g[width]*3+g[-1+width], g[-width]*3+g[-1-width]) / 4;
    }
    free(grayscale);

    if(global_snoop->gradient_normal){
	// normalization
	int peak = 0;
	const int padding = rowstride - width*channels;

	p = pixels;
	for(i = 0; i < height; i++){
	    for(j = 0; j < width; j++){
		if(peak < p[0]) peak = p[0];
		p += channels;
	    }
	    p += padding;
	}
	if(peak > 0){
	    p = pixels;
	    for(i = 0; i < height; i++){
		for(j = 0; j < width; j++){
		    p[0] = p[1] = p[2] = p[0]*255/peak;
		    p += channels;
		}
		p += padding;
	    }
	}
    }
}
