#include <cmath>
#include <algorithm>

#include "image.h"

static void rgb2hsv(guchar* p, float* hsv) // {{{
{
	float r = p[0]/255.0, g = p[1]/255.0, b = p[2]/255.0;
	float max = r > g ? (r > b ? r : b) : (g > b ? g : b);
	float min = r < g ? (r < b ? r : b) : (g < b ? g : b);
	float diff = max - min;

	// H
	if (max == min)
		hsv[0] = 0;
	else if (max == r) 
		hsv[0] = 60 * (g-b)/diff;
	else if (max == g)
		hsv[0] = 60 * (b-r)/diff + 120;
	else if (max == b)
		hsv[0] = 60 * (r-g)/diff + 240;
	if (hsv[0] < 0) hsv[0] += 360;

	// S
	hsv[1] = (max == 0) ? 0 : diff/max;

	// V
	hsv[2] = max;
} // }}}

void
update_status(GdkPixbuf *pixbuf, Geometry *g) // {{{
{
	int x = g->center_x, y = g->center_y;
	guchar *p = gdk_pixbuf_get_pixels(pixbuf) + 
		y*gdk_pixbuf_get_rowstride(pixbuf) + x*gdk_pixbuf_get_n_channels(pixbuf);
	int pointer_y = g->pointer_y;
	
	/* coordinate */
	if (global_snoop->inverse_y)
		pointer_y = g->screen_height - pointer_y - 1;
	char coord[20] = {0};
	sprintf(coord, "(%04d, %04d)", g->pointer_x, pointer_y);

	/* color */
	// if (global_snoop->hex_mode) 
	char color[30] = {0};
	switch (global_snoop->colorspace) {
		case RGB:
			sprintf(color, "(%03d, %03d, %03d)", (int)p[0], (int)p[1], (int)p[2]);
			break;
		case RGB_HEX:
			sprintf(color, "(%02x, %02x, %02x)", (int)p[0], (int)p[1], (int)p[2]);
			break;
		case HSV: {
			float hsv[3] = {0};
			rgb2hsv(p, hsv);
			sprintf(color, "(%7.3f, %.3f, %.3f)", hsv[0], hsv[1], hsv[2]);
			break;
		}
	}

	char msg[200] = {0};
	sprintf(msg, "%s - %s", coord, color);

	// gtk_statusbar_pop((GtkStatusbar*)global_snoop->statusbar, global_snoop->status_cid);
	guint mid = gtk_statusbar_push(
		(GtkStatusbar*)global_snoop->statusbar, 
		global_snoop->status_cid, msg
	);
	if (global_snoop->status_mid != -1) 
		gtk_statusbar_remove(
			(GtkStatusbar*)global_snoop->statusbar, 
			global_snoop->status_cid, global_snoop->status_mid
		);
	global_snoop->status_mid = mid;
} // }}}

void
prepare_coordinate(Geometry *g) // {{{
{
	Snoop *s = global_snoop;

	// get the display/screen/root window information
	g->display = gdk_display_get_default();
	g->screen = gdk_screen_get_default();
	g->root_window = gdk_get_default_root_window();
	if(!(g->display && g->screen && g->root_window)) {
		// TODO: put some error policy here.
		perror("diplay, screen or root_window allocation failure.");
		exit(1);
	}
	g->screen_width = gdk_screen_width();
	g->screen_height = gdk_screen_height();

	// get the mouse pointer coordinate
	gdk_display_get_pointer(g->display, 0, &(g->pointer_x), &(g->pointer_y), 0);

	g->zoom_factor = s->zoom_factor;
	g->image_width = s->image->allocation.width;
	g->image_height = s->image->allocation.height;
	g->desire_capture_width = std::ceil(static_cast<float>(g->image_width)/g->zoom_factor);
	g->desire_capture_height = std::ceil(static_cast<float>(g->image_height)/g->zoom_factor);

	int shift_x = g->desire_capture_width/2;
	int shift_y = g->desire_capture_height/2;
	int left = g->pointer_x - shift_x;
	int right = left + g->desire_capture_width - 1;
	int top = g->pointer_y - shift_y;
	int bottom = top + g->desire_capture_height - 1;
	g->center_x = shift_x;
	g->center_y = shift_y;
	g->need_fill = g->src_x = g->src_y = 0;
	if (left < 0) {
		g->src_x = -left;
		left = 0;
	}
	if (right >= g->screen_width)
		right = g->screen_width - 1;
	if (top < 0) {
		g->src_y = -top;
		top = 0;
	}
	if (bottom >= g->screen_height)
		bottom = g->screen_height - 1;

	g->capture_left = left;
	g->capture_top = top;
	g->capture_width = right - left + 1;
	g->capture_height = bottom - top + 1;
} // }}}

void 
draw_soft_line(GdkPixbuf *pixbuf, int x1, int y1, int x2, int y2, int r, int g, int b, int alpha) // {{{
{
	cairo_surface_t *surface = cairo_image_surface_create_for_data(
		gdk_pixbuf_get_pixels(pixbuf),
		CAIRO_FORMAT_ARGB32,
		gdk_pixbuf_get_width(pixbuf),
		gdk_pixbuf_get_height(pixbuf),
		gdk_pixbuf_get_rowstride(pixbuf)
	);
	cairo_t *cr = cairo_create(surface);

	double rr = r/255.;
	double gg = g/255.;
	double bb = b/255.;
	double aa = 1;
	if (rr < 0) rr = 0;
	if (gg < 0) gg = 0;
	if (bb < 0) bb = 0;
	if (aa < 0) aa = 0;
	// fprintf(stderr, "cairo_set_source_rgba(%x, %f, %f, %f, %f)\n", cr, rr, gg ,bb, aa);
	cairo_set_source_rgba(cr, rr, gg, bb, aa);
	cairo_set_line_width(cr, 1);
	cairo_set_operator(cr, CAIRO_OPERATOR_ADD);
	cairo_set_line_cap(cr, CAIRO_LINE_CAP_BUTT);
	// cairo_set_antialias(cr, CAIRO_ANTIALIAS_NONE);
	// cairo_set_line_join(cr, CAIRO_LINE_JOIN_BEVEL);

	cairo_move_to(cr, x1, y1);
	cairo_line_to(cr, x2, y2);
	cairo_stroke(cr);
	cairo_destroy(cr);
	cairo_surface_destroy(surface);

} // }}}

void
draw_line(GdkPixbuf *pixbuf, int x1, int y1, int x2, int y2, int r, int g, int b, int alpha) // {{{
{
	if (x1 == x2) {
		draw_vline(pixbuf, x1, y1, y2, r, g, b, alpha);
		return;
	} else if (y1 == y2) {
		draw_hline(pixbuf, y1, x1, x2, r, g, b, alpha);
		return;
	}
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
	guchar *p;

	int dx = x2 - x1;
	int dy = y2 - y1;
	int ax = abs(dx) << 1;
	int ay = abs(dy) << 1;
	int sx = (dx >= 0) ? 1 : -1;
	int sy = (dy >= 0) ? 1 : -1;

	int x = x1;
	int y = y1;

	if (ax > ay) {
		int d = ay - (ax >> 1);
		while (x != x2) {
			// put_pixel(x, y, color);
			p = pixels + y*rowstride + x*n_channels;
			if (r >= 0) p[0] = r;
			if (g >= 0) p[1] = g;
			if (b >= 0) p[2] = b;
			if (alpha >= 0) p[3] = alpha;

			if (d > 0 || (d == 0 && sx == 1)) {
				y += sy;
				d -= ax;
			}
			x += sx;
			d += ay;
		}
	} else {
		int d = ax - (ay >> 1);
		while (y != y2) {
			// put_pixel(x, y, color);
			p = pixels + y*rowstride + x*n_channels;
			if (r >= 0) p[0] = r;
			if (g >= 0) p[1] = g;
			if (b >= 0) p[2] = b;
			if (alpha >= 0) p[3] = alpha;

			if (d > 0 || (d == 0 && sy == 1)) {
				x += sx;
				d -= ay;
			}
			y += sy;
			d += ax;
		}
	}
	// put_pixel(screen, x, y, color);
	p = pixels + y*rowstride + x*n_channels;
	if (r >= 0) p[0] = r;
	if (g >= 0) p[1] = g;
	if (b >= 0) p[2] = b;
	if (alpha >= 0) p[3] = alpha;
} // }}}

void
draw_hline(GdkPixbuf *pixbuf, int y, int x1, int x2, int r, int g, int b, int alpha) // {{{
{
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	if(x1 > x2){
	    // swap x1 and x2
	    int tmp = x1;
	    x1 = x2;
	    x2 = tmp;
	}
	guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
	guchar *p = pixels + y*rowstride + x1*n_channels;
	int x;
	for (x = x1; x <= x2; x++, p+=n_channels) {
		// put_pixel(pixbuf, x, y, r, g, b, alpha);
		if (r >= 0) p[0] = r;
		if (g >= 0) p[1] = g;
		if (b >= 0) p[2] = b;
		if (alpha >= 0) p[3] = alpha;
	}
} // }}}

void
draw_vline(GdkPixbuf *pixbuf, int x, int y1, int y2, int r, int g, int b, int alpha) // {{{
{
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	if(y1 > y2){
	    // swap y1 and y2
	    int tmp = y1;
	    y1 = y2;
	    y2 = tmp;
	}
	guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
	guchar *p = pixels + y1*rowstride + x*n_channels;
	int y;
	for (y = y1; y <= y2; y++, p+=rowstride) {
		//put_pixel(pixbuf, x, y, r, g, b, alpha);
		if (r >= 0) p[0] = r;
		if (g >= 0) p[1] = g;
		if (b >= 0) p[2] = b;
		if (alpha >= 0) p[3] = alpha;
	}
} // }}}

void
fill_color(GdkPixbuf *pixbuf, int r, int g, int b, int alpha) // {{{
{
	int n_channels = gdk_pixbuf_get_n_channels(pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
	int width = gdk_pixbuf_get_width(pixbuf);
	int height = gdk_pixbuf_get_height(pixbuf);
	guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
	int x, y;
	for (y = 0; y < height; y++, pixels += rowstride) {
		guchar *p = pixels;
		for (x = 0; x < width; x++, p += n_channels) {
			if (r >= 0) p[0] = r;
			if (g >= 0) p[1] = g;
			if (b >= 0) p[2] = b;
			if (alpha >= 0) p[3] = alpha;
		}
	}
} // }}}

void
replace_image(GdkPixbuf *base, GdkPixbuf *img, int x0, int y0) // {{{
{
	int n_channels = gdk_pixbuf_get_n_channels(img);
	int width = gdk_pixbuf_get_width(img);
	int height = gdk_pixbuf_get_height(img);

	int base_rowstride = gdk_pixbuf_get_rowstride(base);
	int img_rowstride = gdk_pixbuf_get_rowstride(img);
	guchar *base_pixel = gdk_pixbuf_get_pixels(base);
	guchar *img_pixel = gdk_pixbuf_get_pixels(img);

	base_pixel += y0*base_rowstride;
	for (int y = 0; y < height; y++, img_pixel += img_rowstride, base_pixel += base_rowstride)
		std::copy(
			img_pixel, img_pixel + width*n_channels, 
			base_pixel + x0*n_channels
		);
} // }}}

void 
scale_image(GdkPixbuf *src, GdkPixbuf *dst, int scale) // {{{
{
	int src_width = gdk_pixbuf_get_width(src); 
	int src_height = gdk_pixbuf_get_height(src);
	GdkPixbuf *tmp = gdk_pixbuf_scale_simple(
		src, src_width*scale, src_height*scale, GDK_INTERP_NEAREST
	);

	int dst_width = gdk_pixbuf_get_width(dst);
	int dst_height = gdk_pixbuf_get_height(dst);
	int n_channels = gdk_pixbuf_get_n_channels(dst);
	int dst_rowstride = gdk_pixbuf_get_rowstride(dst);
	int tmp_rowstride = gdk_pixbuf_get_rowstride(tmp);
	guchar *dst_pixel = gdk_pixbuf_get_pixels(dst);
	guchar *tmp_pixel = gdk_pixbuf_get_pixels(tmp);
	int h;
	for (h = 0; h < dst_height; ++h, dst_pixel += dst_rowstride, tmp_pixel += tmp_rowstride)
		std::copy(tmp_pixel, tmp_pixel + dst_width*n_channels, dst_pixel);	
	g_object_unref(tmp);
} // }}}

/*
void
put_pixel (GdkPixbuf *pixbuf, int x, int y, guchar red, guchar green, guchar blue, guchar alpha) // {{{
{
	int width, height, rowstride, n_channels;
	guchar *pixels, *p;

	n_channels = gdk_pixbuf_get_n_channels (pixbuf);

	g_assert (gdk_pixbuf_get_colorspace (pixbuf) == GDK_COLORSPACE_RGB);
	g_assert (gdk_pixbuf_get_bits_per_sample (pixbuf) == 8);
	//g_assert (gdk_pixbuf_get_has_alpha (pixbuf));
	//g_assert (n_channels == 4);

	width = gdk_pixbuf_get_width (pixbuf);
	height = gdk_pixbuf_get_height (pixbuf);

	g_assert (x >= 0 && x < width);
	g_assert (y >= 0 && y < height);

	rowstride = gdk_pixbuf_get_rowstride (pixbuf);
	pixels = gdk_pixbuf_get_pixels (pixbuf);

	p = pixels + y * rowstride + x * n_channels;
	p[0] = red;
	p[1] = green;
	p[2] = blue;
	p[3] = alpha;
} // }}}
*/
