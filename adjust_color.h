#ifndef __ADJUST_COLOR_H__
#define __ADJUST_COLOR_H__

void set_brightnessLUT(int amount);
void set_contrastLUT(int amount);
void set_gammaLUT(double amount);
void adjust_color(GdkPixbuf*);

#endif
