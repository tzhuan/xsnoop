#ifndef __GUI_H__
#define __GUI_H__

#include "snoop.h"
#include "handler.h"
#include "image.h"
#include "draw.h"
#include "graph.h"
#include "adjust_color.h"

void gui_init();
//void update_geometry(void);
void update(void);
void update_title(void);
GtkWidget* menu_init(void);

// key binding handler
gint handle_key(GtkWidget* widget, GdkEventKey* event, gpointer data);

// mouse event handler
gint handle_button_press(GtkWidget* widget, GdkEventButton* button, gpointer data);
gint handle_button_release(GtkWidget* widget, GdkEventButton* button, gpointer data);
gint handle_motion_notify(GtkWidget* widget, GdkEventMotion* motion, gpointer data);

// timer handler
gboolean timer_event(gpointer);

void apply_filter(Filter, GdkPixbuf* src);
void put_ruler();
void unput_ruler();
void update_ruler();
void set_graph_sensitive();

#endif
