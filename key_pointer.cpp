#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <gdk/gdkkeysyms.h>
#include <X11/Xlib.h>
#include "snoop.h"
#include "gui.h"

// move mouse cursor relatively
static void move_cursor(int x, int y)
{
	int orig_x, orig_y;
	gdk_display_get_pointer(gdk_display_get_default(), 0, &orig_x, &orig_y, 0);
	gdk_display_warp_pointer(
		gdk_display_get_default(), gdk_screen_get_default(), orig_x+x, orig_y+y
	);
}

gint handle_key(GtkWidget* widget, GdkEventKey* event, gpointer data){ // {{{
    int key = event->keyval;
    if(key >= '0' && key <= '9'){ // adjust zoom factor
	int zoom = key - '0';
	if(zoom == 0) zoom = 10;
	global_snoop->zoom_factor = zoom;
	update_title();
	update_ruler();
	return TRUE;
    }
    gint handled = FALSE;
    
    switch(key){
	case '+':
	case '=':
	    { 
		// increase zoom factor: 1 2 3 4 6 8 10 16 32
		int zoom = global_snoop->zoom_factor;
		
		if (zoom < 4) ++zoom;
		else if (zoom < 10) zoom = zoom - (zoom&1) + 2;
		else if (zoom == 10) zoom = 16;
		else if (zoom == 16) zoom = 32;

		global_snoop->zoom_factor = zoom;
		update_ruler();
		handled = TRUE;
	    }
	    break;
	    
	case '-':
	    {
		// decrease zoom factor: 32 16 10 8 6 4 3 2 1
		int zoom = global_snoop->zoom_factor;
		
		if (zoom == 32) zoom = 16;
		else if (zoom == 16) zoom = 10;
		else if (zoom > 4) zoom = zoom + (zoom&1) - 2;
		else if (zoom > 1) --zoom;

		global_snoop->zoom_factor = zoom;
		update_ruler();
		handled = TRUE;
	    }
	    break;
	    
	case GDK_Left:
	    move_cursor(-1, 0);
	    handled = TRUE;
	    break;
	    
	case GDK_Right:
	    move_cursor(1, 0);
	    handled = TRUE;
	    break;
	    
	case GDK_Up:
	    move_cursor(0, -1);
	    handled = TRUE;
	    break;
	    
	case GDK_Down:
	    move_cursor(0, 1);
	    handled = TRUE;
	    break;

	default:
	    break;
    }
    if(handled == TRUE)
	update_title();

    return FALSE;
    // short key for menu-item
    Snoop *s = global_snoop;
#define SHORT_KEY_NUM 100
    int index[SHORT_KEY_NUM] = {
	GDK_h, GDK_H, GDK_v, GDK_V, GDK_o, GDK_l, 
	GDK_a, GDK_m, GDK_d, GDK_u, 
	GDK_s, GDK_w, GDK_r, GDK_b,
	GDK_F1, GDK_F2, GDK_F3, GDK_F4, GDK_Tab, GDK_Escape,
	GDK_Page_Up, GDK_Page_Down, GDK_Home, GDK_i
    };
    GtkWidget *item_table[SHORT_KEY_NUM] = {
	s->item_graph[HORIZONTAL_LINEAR], s->item_graph[HORIZONTAL_STEP], s->item_graph[VERTICAL_LINEAR], s->item_graph[VERTICAL_STEP], s->item_graph[NO_GRAPH], s->item_highlight,
	s->item_grab_mode[DRAG_MODE], s->item_grab_mode[MOUSE_ARROW_MODE], NULL, NULL, 
	NULL, NULL, s->item_ruler, NULL,
	NULL, s->item_on_top, NULL/*s->item_grid*/, NULL, NULL,
	NULL, NULL, NULL, NULL
    };
    int i;
    for (i = 0; i < SHORT_KEY_NUM; i++)
	if (key == index[i]) {
		if (item_table[i]) 
			gtk_check_menu_item_set_active(
				(GtkCheckMenuItem*)item_table[i], 
				!gtk_check_menu_item_get_active(
					(GtkCheckMenuItem*)item_table[i]));
		handled = TRUE;
		break;
	}

    return handled;
} // }}}

gint
handle_button_press(GtkWidget* widget, GdkEventButton* button, gpointer data) // {{{
{
	if (global_snoop->grab_mode == MOUSE_ARROW_MODE)
		return TRUE;
	if (gdk_pointer_is_grabbed() == FALSE) {
		Snoop *s = global_snoop;
		int width = s->image->allocation.width/s->zoom_factor + 2;
		int height = s->image->allocation.height/s->zoom_factor + 2;
		GdkPixbuf *pixbuf = gdk_pixbuf_new(
				GDK_COLORSPACE_RGB, TRUE, 8, width, height);
		fill_color(pixbuf, 0, 0, 0, 0);

		draw_line(pixbuf, 0, 0, width-1, 0, 255, 255, 255, 255);
		draw_line(pixbuf, 0, height-1, width-1, height-1, 255, 255, 255, 255);
		draw_line(pixbuf, 0, 0, 0, height-1, 255, 255, 255, 255);
		draw_line(pixbuf, width-1, 0, width-1, height-1, 255, 255, 255, 255);

		draw_line(pixbuf, 1, 1, width-2, 1, 0, 0, 0, 255);
		draw_line(pixbuf, 1, height-2, width-2, height-2, 0, 0, 0, 255);
		draw_line(pixbuf, 1, 1, 1, height-2, 0, 0, 0, 255);
		draw_line(pixbuf, width-2, 1, width-2, height-2, 0, 0, 0, 255);

		GdkCursor *cursor = gdk_cursor_new_from_pixbuf(
			gdk_display_get_default(), pixbuf, width/2, height/2); 

		gdk_pointer_grab( global_snoop->main_window->window, TRUE, 
			GDK_BUTTON_RELEASE_MASK, NULL, cursor, GDK_CURRENT_TIME);	
		g_object_unref(pixbuf);
		gdk_cursor_unref(cursor);
		global_snoop->timer_state = TIMER_RUNNING;
		g_timeout_add(global_snoop->refresh_interval, timer_event, NULL);
	}
	return FALSE;
} // }}}

gint
handle_button_release(GtkWidget* widget, GdkEventButton* button, gpointer data) // {{{
{
	if (global_snoop->grab_mode == MOUSE_ARROW_MODE)
		return TRUE;
	if (gdk_pointer_is_grabbed() == TRUE) {
		gdk_pointer_ungrab(GDK_CURRENT_TIME);
		global_snoop->timer_state = TIMER_STOP;
	}
	return FALSE;
} // }}}

gint
handle_motion_notify(GtkWidget* widget, GdkEventMotion* motion, gpointer data) // {{{
{
	Snoop *s = global_snoop;
	gdouble lower, upper, max;

	gtk_ruler_get_range((GtkRuler*)s->hruler, &lower, &upper, NULL, &max);
	gtk_ruler_set_range((GtkRuler*)s->hruler, lower, upper, 
		motion->x/s->zoom_factor, max);

	gtk_ruler_get_range((GtkRuler*)s->vruler, &lower, &upper, NULL, &max);
	gtk_ruler_set_range((GtkRuler*)s->vruler, lower, upper, 
		motion->y/s->zoom_factor, max);

	return TRUE;
} // }}}

