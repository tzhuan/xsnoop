#include "snoop.h"

ENUM_OPERATOR_IN_DEFINITION(TimerState);
ENUM_OPERATOR_IN_DEFINITION(Colorspace);
ENUM_OPERATOR_IN_DEFINITION(Filter);
ENUM_OPERATOR_IN_DEFINITION(GrabMode);
ENUM_OPERATOR_IN_DEFINITION(Graph);

Snoop*
snoop_new()
{
	int error_flag;
	Snoop *s = (Snoop*)malloc(sizeof(Snoop));
	if (!s) {
		// TODO: put some error policy here.
		error_flag = 1;
		perror("snoop memory allocation failure.");
		return NULL;
	}

	snoop_set_default(s);

	s->main_window = NULL;
	s->image = NULL;
	s->statusbar = NULL;

	s->item_on_top = NULL;
	s->item_grid = NULL;
	s->item_hex_mode = NULL;
	s->item_inverse_y = NULL;
	s->item_center_highlight = NULL;
	s->item_highlight = NULL;
	s->item_filter = NULL;
	s->item_grab_mode = NULL;
	s->item_graph = NULL;

	s->status_cid = -1;
	s->status_mid = -1;

	return s;
}

void
snoop_set_default(Snoop* s){
	s->zoom_factor = 4;
	s->refresh_interval = 100;
	s->on_top = false;
	s->update = true;
	s->grid = false;
	s->hex_mode = false;
	s->inverse_y = false;
	s->highlight = true;
	s->center_highlight = true;
	s->filter = NO_FILTER;
	s->graph = NO_GRAPH;
	s->statistics_info = false;
	s->ruler = false;
	s->grab_mode = MOUSE_ARROW_MODE;
	s->width = 300;
	s->height = 300;
	s->red = true;
	s->green = true;
	s->blue = true;
	s->histogram_normal = false;
	s->gradient_normal = false;
	s->histogram_reference = 5000;
	s->gradient_reference = 255;
	s->brightness = 0;
	s->contrast = 0;
	s->gamma = 1.0;
	s->colorspace = RGB;
}

void
snoop_destroy(Snoop *s)
{
	free(s->item_filter);
	free(s->item_grab_mode);
	free(s->item_graph);
	free(s);
}
