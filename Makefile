CC=	gcc
CXX=g++
CFLAGS=	$(shell pkg-config --cflags gtk+-2.0 glib-2.0 pango) -O3 -Wall
LIBS=	$(shell pkg-config --libs gtk+-2.0 glib-2.0 pango)
CXXFLAGS=$(CFLAGS)
TARGET=	snoop
OBJS=	main.o snoop.o gui.o key_pointer.o image.o filter.o menu.o \
	handler.o config.o graph.o draw.o adjust_color.o
JUNK=	$(TARGET) $(OBJS)

$(TARGET): $(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)
	strip $(TARGET)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -rf $(TARGET) $(OBJS) *.core

main.h: snoop.h gui.h
main.o: main.h
snoop.o: snoop.h
gui.h: snoop.h handler.h image.h draw.h graph.h adjust_color.h
gui.o: snoop.h gui.h image.h draw.h graph.h handler.h
key_pointer.o: snoop.h gui.h
image.h: snoop.h
image.o: snoop.h image.h
graph.o: snoop.h graph.h
draw.h: image.h
draw.o: snoop.h draw.h
filter.o: snoop.h gui.h
menu.o: snoop.h gui.h
handler.o: snoop.h handler.h gui.h
config.o: snoop.h
adjust_color.o: snoop.h adjust_color.h
draw.h: image.h snoop.h
draw.o: draw.h
