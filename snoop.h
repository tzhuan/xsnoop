#ifndef __SNOOP_H__
#define __SNOOP_H__

#include <stdio.h>
#include <stdlib.h>

#include <istream>
#include <string>
#include <gtk/gtk.h>

#define ENUM_OPERATOR_IN_DECLARATION(TYPE) \
	std::istream& operator >>(std::istream&, TYPE&);

#define ENUM_OPERATOR_IN_DEFINITION(TYPE) \
	std::istream& operator >>(std::istream& is, TYPE& cs) \
	{ \
		int v = 0; \
		is >> v; \
		cs = static_cast<TYPE>(v); \
		return is; \
	}

typedef enum { TIMER_RUNNING, TIMER_STOP, TIMER_RESTART } TimerState;
typedef enum { RGB, RGB_HEX, HSV } Colorspace;
typedef enum { NO_FILTER, GRADIENT } Filter;
typedef enum { MOUSE_ARROW_MODE, DRAG_MODE} GrabMode;

typedef enum {
	NO_GRAPH, HORIZONTAL_STEP, HORIZONTAL_LINEAR, 
	VERTICAL_STEP, VERTICAL_LINEAR, STATISTICS
} Graph;

ENUM_OPERATOR_IN_DECLARATION(TimerState);
ENUM_OPERATOR_IN_DECLARATION(Colorspace);
ENUM_OPERATOR_IN_DECLARATION(Filter);
ENUM_OPERATOR_IN_DECLARATION(GrabMode);
ENUM_OPERATOR_IN_DECLARATION(Graph);

typedef struct { // {{{
	// option variable 
	int zoom_factor;

	bool autosave;
	bool on_top;
	bool update;
	bool grid;
	bool ruler;
	bool hex_mode;
	bool inverse_y;
	// a red square in the center
	bool center_highlight;
	bool red, green, blue;
	bool histogram_normal, gradient_normal;
	int histogram_reference, gradient_reference;
	int brightness, contrast;
	double gamma;
	unsigned char brightnessLUT[256], contrastLUT[256], gammaLUT[256];


	// a half-transparent band in the center when display graph
	bool highlight;
	// statistics window
	bool statistics_info;
	Colorspace colorspace;
	Filter filter;
	Graph graph;
	GrabMode grab_mode;

	int refresh_interval;	// in msecond (0.001 second)
	int width;
	int height;
	TimerState timer_state;

	GtkWidget *main_window;
	GtkWidget *time_window;
	GtkWidget *display_window;
	GtkWidget *about_window;

	GtkWidget *image, *stat_image;
	GtkWidget *statusbar;
	GtkWidget *hruler, *vruler;
	guint status_cid;
	guint status_mid;

	GtkWidget *item_refresh_interval;
	GtkWidget *item_red, *item_green, *item_blue;
	GtkWidget *item_histogram_normal, *item_gradient_normal;
	GtkWidget *item_histogram_reference, *item_gradient_reference;
	GtkWidget *item_brightness, *item_contrast, *item_gamma;
	// labels in statistics window
	GtkWidget *item_peak, *item_minmax, *item_avgmed;

	GtkWidget *item_on_top;
	GtkWidget *item_grid;
	GtkWidget *item_hex_mode;
	GtkWidget *item_inverse_y;
	GtkWidget *item_center_highlight;
	GtkWidget *item_highlight;
	GtkWidget *item_ruler;
	GtkWidget **item_colorspace;
	GtkWidget **item_filter;
	GtkWidget **item_grab_mode;
	GtkWidget **item_graph;
	GtkWidget *item_statistics_info;

	GtkWidget *item_quit;
	GtkWidget *item_update;

	GtkAccelGroup *accel_group;
} Snoop; // }}}

Snoop *snoop_new(void);
// set all options to default values
void snoop_set_default(Snoop* s);
void snoop_destroy(Snoop*);

// read/write config file
bool load_config(std::string = "");
bool save_config(std::string = "");

extern Snoop *global_snoop;
extern const guint8 snoop_icon_pixbuf[];

#endif
