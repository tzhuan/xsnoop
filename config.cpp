#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sstream>

#include <glib.h>
#include "snoop.h"

using namespace std;

namespace { // {{{

string get_default_config_file(void) // {{{
{
	string filename = g_get_user_config_dir();
	filename += "/snoop";
	return filename;
} // }}}

template<typename Type>
void save_option(ostream& os, const char* key, Type& value) // {{{
{
	ostringstream oss;
	oss << key << "=" << value << endl;
	os << oss.str();
	if (!os) 
		throw runtime_error(string("save option ") + oss.str() + " fail.");
} // }}}

template<typename Type>
void load_option(GKeyFile* file, const char* key, Type& value) // {{{
{
    gchar* string = g_key_file_get_value(file, "snoop", key, 0);
    if (!string) return;
    istringstream iss(string);
    iss >> value;
    // cerr << string << " => " << value << endl;
    g_free(string);
} // }}}

} // end of anonymous namesapce }}}

bool load_config(string filename) // {{{
{
	if(filename == "") // construct filename: ~/.config/snooprc
		filename = get_default_config_file();

    GKeyFile *file = 0;
    try {
        file = g_key_file_new();
        if (!file)
            throw runtime_error("g_key_file_new() fail");

        GError *error = 0;
        if (!g_key_file_load_from_file(file, filename.c_str(), G_KEY_FILE_NONE, &error)) {
            string msg = "g_key_file_load_from_file() fail: ";
            msg += error->message;
            g_error_free(error);
            throw runtime_error(msg.c_str());
        }

		Snoop* s = global_snoop;
		// load options {{{
		load_option(file, "autosave", s->autosave);
		load_option(file, "zoom_factor", s->zoom_factor);
		load_option(file, "on_top", s->on_top);
		load_option(file, "update", s->update);
		load_option(file, "grid", s->grid);
		load_option(file, "red", s->red);
		load_option(file, "green", s->green);
		load_option(file, "blue", s->blue);
		load_option(file, "ruler", s->ruler);
		load_option(file, "hex_mode", s->hex_mode);
		load_option(file, "inverse_y", s->inverse_y);
		load_option(file, "center_highlight", s->center_highlight);
		load_option(file, "highlight", s->highlight);
		load_option(file, "colorspace", s->colorspace);
		load_option(file, "filter", s->filter);
		load_option(file, "graph", s->graph);
		load_option(file, "statistics_info", s->statistics_info);
		load_option(file, "grab_mode", s->grab_mode);
		load_option(file, "refresh_interval", s->refresh_interval);
		load_option(file, "width", s->width);
		load_option(file, "height", s->height);
		load_option(file, "histogram_normal", s->histogram_normal);
		load_option(file, "histogram_reference", s->histogram_reference);
		load_option(file, "gradient_normal", s->gradient_normal);
		load_option(file, "gradient_reference", s->gradient_reference);
		load_option(file, "brightness", s->brightness);
		load_option(file, "contrast", s->contrast);
		load_option(file, "gamma", s->gamma);
		// }}}
    }
    catch (exception& e) {
        cerr << e.what() << endl;
        if (file) g_key_file_free(file);
        return false;
    }
    if (file)
        g_key_file_free(file);

    return true;

	cerr << "load config from " << filename << endl;
	return true;
} // }}}

bool save_config(string filename) // {{{
{
	if(filename == "")
		filename = get_default_config_file();

	try {
		ofstream ofs(filename.c_str(), ios_base::out | ios_base::trunc);
		if (!ofs)
			throw runtime_error("save config file " + filename + " fail.");

		Snoop* s = global_snoop;
		// save options {{{
		ofs << "[snoop]" << endl;
		save_option(ofs, "autosave", s->autosave);
		save_option(ofs, "zoom_factor", s->zoom_factor);
		save_option(ofs, "on_top", s->on_top);
		save_option(ofs, "update", s->update);
		save_option(ofs, "grid", s->grid);
		save_option(ofs, "red", s->red);
		save_option(ofs, "green", s->green);
		save_option(ofs, "blue", s->blue);
		save_option(ofs, "ruler", s->ruler);
		save_option(ofs, "hex_mode", s->hex_mode);
		save_option(ofs, "inverse_y", s->inverse_y);
		save_option(ofs, "center_highlight", s->center_highlight);
		save_option(ofs, "highlight", s->highlight);
		save_option(ofs, "colorspace", s->colorspace);
		save_option(ofs, "filter", s->filter);
		save_option(ofs, "graph", s->graph);
		save_option(ofs, "statistics_info", s->statistics_info);
		save_option(ofs, "grab_mode", s->grab_mode);
		save_option(ofs, "refresh_interval", s->refresh_interval);
		save_option(ofs, "width", s->width);
		save_option(ofs, "height", s->height);
		save_option(ofs, "histogram_normal", s->histogram_normal);
		save_option(ofs, "histogram_reference", s->histogram_reference);
		save_option(ofs, "gradient_normal", s->gradient_normal);
		save_option(ofs, "gradient_reference", s->gradient_reference);
		save_option(ofs, "brightness", s->brightness);
		save_option(ofs, "contrast", s->contrast);
		save_option(ofs, "gamma", s->gamma);
		// }}}
	} 
	catch (exception& e) {
		cerr << e.what() << endl;
		return false;
	}
	return true;
} // }}}
