#ifndef __DRAW_H__
#define __DRAW_H__

#include "snoop.h"
#include "image.h"

void draw_grid(GdkPixbuf*, int);
void draw_center(GdkPixbuf *, int, int);
void draw_hhighlight(GdkPixbuf *pixbuf, Geometry *g);
void draw_vhighlight(GdkPixbuf *pixbuf, Geometry *g);

#endif
