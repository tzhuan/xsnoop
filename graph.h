#ifndef __GRAPH_H__
#define __GRAPH_H__

// generic version: calls the proper draw_XXgraph()
// according to the graph option in global_snoop
void draw_graph(GdkPixbuf*, GdkPixbuf*, int, int);

// draw horizontal stepping graph
void draw_hsgraph(GdkPixbuf *src, GdkPixbuf *pixbuf, int, int top, int left, int right, int bottom);

// draw horizontal linear graph
void draw_hlgraph(GdkPixbuf *src, GdkPixbuf *pixbuf, int, int top, int left, int right, int bottom);

// draw vertical stepping graph
void draw_vsgraph(GdkPixbuf *src, GdkPixbuf *pixbuf, int, int top, int left, int right, int bottom);

// draw vertical linear graph
void draw_vlgraph(GdkPixbuf *src, GdkPixbuf *pixbuf, int, int top, int left, int right, int bottom);

// update statistics chart
void update_statistics(Geometry *g, GdkPixbuf *src, GdkPixbuf *dst);

#endif
