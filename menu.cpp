#include "snoop.h"
#include "gui.h"

static GtkWidget* menu_edit_init(void);
static GtkWidget* menu_config_init(void);
static GtkWidget* menu_colorspace_init(void);
static GtkWidget* menu_filter_init(void);
static GtkWidget* menu_graph_init(void);
static void add_separator(GtkWidget *menu);
static GtkWidget* add_check_menu_item(GtkWidget *, const gchar *, 
	void (*)(GtkCheckMenuItem*, gpointer), gpointer, gboolean);
static GtkWidget** add_radio_menu_item_group(GtkWidget *, int, 
	const gchar (*)[32], void (*)(GtkRadioMenuItem*, gpointer), int);
static void add_menu_item(GtkWidget *, const gchar *, 
	void (*)(GtkMenuItem*, gpointer), gpointer);

GtkWidget*
menu_init(void) // {{{
{
	GtkWidget *menubar = gtk_menu_bar_new();
	gtk_menu_bar_append(GTK_MENU_BAR(menubar), menu_edit_init());
	gtk_menu_bar_append(GTK_MENU_BAR(menubar), menu_config_init());
	gtk_menu_bar_append(GTK_MENU_BAR(menubar), menu_colorspace_init());
	gtk_menu_bar_append(GTK_MENU_BAR(menubar), menu_filter_init());
	gtk_menu_bar_append(GTK_MENU_BAR(menubar), menu_graph_init());
	return menubar;
} // }}}

GtkWidget*
menu_edit_init(void) // {{{
{
	GtkWidget *menu;
	GtkWidget *menuitem;
	Snoop *s = global_snoop;

	menu = gtk_menu_new();

	/* FIXME: upgrade gtk to 2.6 to turn on this feature.
	menuitem = gtk_menu_item_new_with_mnemonic("_Copy");
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
	g_signal_connect(G_OBJECT(menuitem), "activate", 
			G_CALLBACK(handle_copy), NULL);
	add_separator(menu);
	*/

	menuitem = s->item_update = gtk_menu_item_new_with_mnemonic("_Update");
	g_signal_connect(G_OBJECT(menuitem), "activate", 
			G_CALLBACK(handle_update), NULL);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);

	add_separator(menu);

	menuitem = gtk_menu_item_new_with_mnemonic("_Save Config");
	g_signal_connect(G_OBJECT(menuitem), "activate", 
			G_CALLBACK(handle_saveconfig), NULL);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);

	add_check_menu_item(menu, "Auto Save Config", handle_option,
				&(s->autosave), s->autosave);

	//add_separator(menu);

	menuitem = gtk_menu_item_new_with_mnemonic("_About...");
	g_signal_connect(G_OBJECT(menuitem), "activate", 
			G_CALLBACK(handle_about), NULL);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);

	add_separator(menu);

	menuitem = s->item_quit = gtk_menu_item_new_with_mnemonic("_Quit");
	g_signal_connect(G_OBJECT(menuitem), "activate", 
			G_CALLBACK(handle_quit), NULL);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);

	menuitem = gtk_menu_item_new_with_mnemonic("_Edit");
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), menu);
	return menuitem;
} // }}}

GtkWidget*
menu_config_init(void) // {{{
{
	GtkWidget *menu;
	GtkWidget *menuitem;
	Snoop *s = global_snoop;

	menu = gtk_menu_new();

	add_menu_item(menu, "_Refresh Rate...", 
		handle_refresh_rate_dialog, NULL);

	/* TODO
	 * display a dialog with many options.
	 *
	 * add_menu_item(menu, "_Display Options...", NULL, NULL);
	 */
	add_menu_item(menu, "_Display Options...", 
		handle_display_option_dialog, NULL);

	s->item_on_top = add_check_menu_item(menu, "_Always On Top", 
				handle_ontop, NULL, s->on_top);

	s->item_grid = add_check_menu_item(menu, "_Grid", handle_option,
				&(s->grid), s->grid);

	s->item_ruler = add_check_menu_item(menu, "_Ruler", handle_ruler,
				&(s->ruler), FALSE);

	s->item_center_highlight = add_check_menu_item(menu, 
		"_Center Highlight", handle_option, &(s->center_highlight), 
		s->center_highlight);

	add_separator(menu);

	/* TODO: 
	 * Screen: calculate coordinate by root_window.
	 * Active window: calculate coordinate by focused window.
	 * not sure this feature supported by GTK.
	 *
	const gchar coord_labels[2][32] = {"_Screen", "Active _Window"};
	add_radio_menu_item_group(menu, 2, coord_labels, NULL, NULL);
	 */

	s->item_inverse_y = add_check_menu_item(menu, "Inverse _Y", 
		handle_option, &(s->inverse_y), s->inverse_y);

	/* move to Color menu
	s->item_hex_mode = add_check_menu_item(menu, "He_x Mode", 
		handle_option, &(s->hex_mode), s->hex_mode);
	*/

	/* TODO
	 * bound when cursor move to four sides, 
	 * only affect when multi-monitor.
	 */
	//add_check_menu_item(menu, "_Bound", , &(global_snoop->), global_snoop-> == ENABLE);

	add_separator(menu);

	const gchar mode_labels[2][32] = {"_Mouse/Arrow Mode", "Drag Mode"};
	s->item_grab_mode = add_radio_menu_item_group(menu, 2, mode_labels, 
				handle_grab_mode, s->grab_mode);

	menuitem = gtk_menu_item_new_with_mnemonic("_Config");
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), menu);
	return menuitem;
} // }}}

GtkWidget*
menu_colorspace_init(void) // {{{
{
	GtkWidget *menu;
	GtkWidget *menuitem;
	Snoop *s = global_snoop;

	menu = gtk_menu_new();

	const gchar colorspace_labels[][32] = {"RGB", "RGB (hex)", "HSV"};
	s->item_colorspace = add_radio_menu_item_group(menu, 3, colorspace_labels, 
				handle_colorspace, s->colorspace);

	menuitem = gtk_menu_item_new_with_mnemonic("Colorspace");
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), menu);
	return menuitem;
} // }}}

GtkWidget*
menu_filter_init(void) // {{{
{
	GtkWidget *menu;
	GtkWidget *menuitem;
	Snoop *s = global_snoop;

	menu = gtk_menu_new();

	const gchar filter_labels[][32] = {"_None", "Gradient"};
	s->item_filter = add_radio_menu_item_group(menu, 2, filter_labels, 
				handle_filter, s->filter);

	menuitem = gtk_menu_item_new_with_mnemonic("_Filter");
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), menu);
	return menuitem;
} // }}}

GtkWidget*
menu_graph_init(void) // {{{
{
	GtkWidget *menu;
	GtkWidget *menuitem;
	Snoop *s = global_snoop;

	menu = gtk_menu_new();

	const gchar graph_labels[][32] = { 
		"Graph _Off", 
		"_Horizontal Step", "Horizontal Linear", 
		"_Vertical Step", "Vertical Linear", 
		"_Statistics"
	};
	s->item_graph = add_radio_menu_item_group(menu, 6, graph_labels, 
				handle_graph, s->graph);

	add_separator(menu);

	s->item_highlight = add_check_menu_item(menu, "_Highlight", 
			handle_option, &(s->highlight), s->highlight);

	s->item_statistics_info = add_check_menu_item(menu, "Statistics _Info", 
			handle_statistics_info, NULL, s->statistics_info);

	menuitem = gtk_menu_item_new_with_mnemonic("_Graph");
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuitem), menu);

	set_graph_sensitive();
	return menuitem;
} // }}}

void
add_separator(GtkWidget *menu) // {{{
{
	GtkWidget *item = gtk_separator_menu_item_new();
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
} // }}}

GtkWidget*
add_check_menu_item(GtkWidget *menu, const gchar *label, // {{{
	void (*handler)(GtkCheckMenuItem*, gpointer), gpointer data, 
	gboolean active) 
{
	GtkWidget *menuitem = gtk_check_menu_item_new_with_mnemonic(label);
	g_signal_connect(G_OBJECT(menuitem), "toggled", 
		G_CALLBACK(handler), data);
	gtk_check_menu_item_set_active((GtkCheckMenuItem*)menuitem, active);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
	return menuitem;
} // }}}

GtkWidget**
add_radio_menu_item_group(GtkWidget *menu, int num, const gchar (*label)[32], 
	void (*handler)(GtkRadioMenuItem*, gpointer), int active) // {{{
{
	GtkWidget **item = (GtkWidget**)malloc(sizeof(GtkWidget*)*num);
	GSList *group = NULL;

	long i;
	for (i = 0; i < num; i++) {
		item[i] = gtk_radio_menu_item_new_with_mnemonic(
				group, label[i]);
		group = gtk_radio_menu_item_get_group(
				GTK_RADIO_MENU_ITEM(item[i]));
		g_signal_connect(G_OBJECT(item[i]), "toggled", 
			G_CALLBACK(handler), item+i);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item[i]);
	}
	gtk_check_menu_item_set_active((GtkCheckMenuItem*)item[active], TRUE);
	return item;
} // }}}

void
add_menu_item(GtkWidget *menu, const gchar *label, // {{{
	void (*handler)(GtkMenuItem*, gpointer), gpointer data) 
{
	GtkWidget *menuitem = gtk_menu_item_new_with_mnemonic(label);
	g_signal_connect(G_OBJECT(menuitem), "activate", 
			G_CALLBACK(handler), NULL);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu), menuitem);
} // }}}
